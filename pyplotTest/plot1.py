import matplotlib.pyplot as plt
import matplotlib
import numpy as np
from math import *
import os

from matplotlib.backends.backend_pdf import PdfPages

def tex(entry):
    return '$'+entry+'$'

x1 = 0;
x2 = 2*pi;
npoints = 100
x = np.linspace(x1,x2, num=npoints, endpoint=True)

font = {'family':'serif','serif':'Computer Modern Roman','size':'14'}
matplotlib.rc('font',**font)
matplotlib.rc('text',usetex='true')

plt.plot(x,map(cos,x),x,map(sin,x))

plt.ylabel(tex('\mathrm{sin}(x) \ [-]'))
plt.xlabel(tex('x \ [m]'))
plt.axis([x1,x2,-1,1])

out = PdfPages('plot1.pdf')
plt.savefig(out,format='pdf')
out.close()

os.system("okular "+"plot1.pdf"+"&")


