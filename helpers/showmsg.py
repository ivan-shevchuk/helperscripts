#! /bin/python

import wxversion
wxversion.select('3.0')
import wx
from optparse import OptionParser

########################################################################
class MyFrame(wx.Frame):
    """"""
    #----------------------------------------------------------------------
    def __init__(self,windowtitle):
        """Constructor"""
        wx.Frame.__init__(self, None, title=windowtitle)
        panel = wx.Panel(self)

        closeBtn = wx.Button(panel, label="Close")
        closeBtn.Bind(wx.EVT_BUTTON, self.onClose)
        closeBtn.Centre()

        button_sizer1 = wx.BoxSizer(wx.HORIZONTAL)     #add button1 to sizer
        button_sizer2 = wx.BoxSizer(wx.VERTICAL)     #add button2 to sizer
        button_sizer1.Add(button_sizer2, 1, wx.ALIGN_CENTER)
        button_sizer2.Add(closeBtn, 0, wx.ALIGN_CENTER)

        panel.SetSizer(button_sizer1)          #set sizers for panel
 

    #----------------------------------------------------------------------
    def onClose(self, event):
        """"""
        self.Close()

def msg(windowtitle):

    app = wx.App(False)
    frame = MyFrame(windowtitle)
    frame.Centre()
    frame.Show()
    app.MainLoop()

'''
  app = wx.App(redirect=False)

  frame = wx.Frame(None, title=windowtitle, size=(300,200))
  panel = wx.Panel(frame, -1)
  text1 = text
  font1 = wx.Font(10, wx.NORMAL, wx.ITALIC, wx.NORMAL)
  lyrics1 = wx.StaticText(panel, -1, text1,(30,15), style=wx.ALIGN_CENTRE)

  frame.Center()
  frame.Show(True)

  app.MainLoop()
'''

if __name__ == "__main__":

    parser = OptionParser()

    parser.add_option("-m","--message",dest="msg",type="string",help="Message to show",metavar="MSG",default="Plots are ready")

    (options, args) = parser.parse_args()    

    msg(options.msg)
