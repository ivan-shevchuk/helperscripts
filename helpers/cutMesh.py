#! /bin/python
# edits the converted

import sys
import os
from os import path
from os.path import join
from math import *
from quaternion import *
import subprocess as sp
import numpy as np
import cutWithPlane
import argparse
import pvopenstate
import glob as gb


from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

defaultCutMeshDictPath = "system/cutMeshDict"

#clearMesh=sys.argv[1]

def main(case,dictPath=defaultCutMeshDictPath):

    caseAbs=os.path.abspath(case)
    
    if os.path.isabs(case):
        print "case is abs"
    
    topoSetDictMainRel = "system/topoSetDictMain"
    topoSetDictBankRel = "system/topoSetDictBank"
    
    topoSetDictMain = join(case,topoSetDictMainRel)
    topoSetDictBank = join(case,topoSetDictBankRel)
    subPath=join(case,"submesh")
    
    topoSetDictTemplatePath = join(case,"system/topoSetDict.template")
   
    sp.call(["rm",topoSetDictMain])
    sp.call(["cp",topoSetDictTemplatePath,topoSetDictMain])
    sp.call(["rm",topoSetDictBank])
    sp.call(["cp",topoSetDictTemplatePath,topoSetDictBank])

    tsdMain  = ParsedParameterFile(topoSetDictMain)
    actsMain = tsdMain["actions"]
    tsdBank  = ParsedParameterFile(topoSetDictBank)
    actsBank = tsdBank["actions"]
    
    cellSetName = actsMain[0]["name"]
    

    # box2p2  box1p2    
    #    |     |      |
    #    |     |     /\
    #    |box2-|box1 ||here is the ship
    #    |     |      |
    #    |     |      |
    #        box2p1     box1p1
    
    splittingBoxDict = ParsedParameterFile(join(case,dictPath),noHeader=True)["splittingParameters"]
    
    domainBox = splittingBoxDict["domainBox"]
    ySplit    = splittingBoxDict["ySplit"]
    
    box1p1    = domainBox["p1"]
    box1p2    = [domainBox["p2"][0], ySplit - 0.01, domainBox["p2"][2]]
    
    box2p1    = [domainBox["p1"][0], ySplit + 0.01, domainBox["p1"][2]]
    box2p2    = domainBox["p2"]
    
    #if debugMode
    print "box1: ", box1p1, " ", box1p2
    print "box2: ", box2p1, " ", box2p2
    
    actsMain[0]["sourceInfo"]["box"][0] = box1p1
    actsMain[0]["sourceInfo"]["box"][1] = box1p2
    actsBank[0]["sourceInfo"]["box"][0] = box2p1
    actsBank[0]["sourceInfo"]["box"][1] = box2p2

    tsdMain.writeFile()
    tsdBank.writeFile()
    
    sp.call(["createPatch","-dict","system/createPatchDictBaffles","-overwrite","-case",case])
    
    if os.path.exists(subPath):
        sp.call(["rm","-r",subPath])
    sp.call(["mkdir",subPath])
    
    sp.call(["cp","-r"]+map(lambda a: join(case,a), ["0","system","constant"])+[subPath])
    
    
    sp.call(["topoSet","-dict",topoSetDictMainRel,"-case",case])
    sp.call(["subsetMesh","-overwrite",cellSetName,"-patch","bankSplit1","-case",case])
    #sp.call(["foamToVTK"])
    
    sp.call(["topoSet","-dict",topoSetDictBankRel,"-case",subPath])
    sp.call(["subsetMesh","-overwrite",cellSetName,"-patch","bankSplit2","-case",subPath])

    
    cutWithPlane.main(subPath)
    sp.call(["foamToVTK","-case",subPath])
    
    #pvopenstate("--state=watchSplit.pvsm")
    #sp.call(["paraview","--state=watchSplit.pvsm"])
    
    sp.call(["mergeMeshes","-overwrite",case,subPath])
    sp.call(["stitchMesh","-overwrite","bankSplit1","bankSplit2","-case",case])
    #sp.call(["foamToVTK"])
    
    additional(case)
    
def additional(case):

    rmlst = gb.glob(join(case,"Ply*"))# + ["subMesh"]
    sp.call(["rm","-r"]+rmlst)
    sp.call(["createPatch","-dict","system/createPatchDict","-overwrite","-case",case])
    sp.call(["cp","-r",join(case,"constant/polyMesh"),join(case,"constant/polyMesh_cut")])

#--------EXE

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument("--case",dest="case",help="Folder, containing the mesh to be cut",metavar="CASE",default=".")
    parser.add_argument("--dict",dest="dictPath",help="cutMeshDict path",metavar="PATH",default=defaultCutMeshDictPath)
    
    args = parser.parse_args()
    
    main(args.case,args.dictPath)
    
