#! /bin/python
# quaternion base class for rotation of a vector about any other vector

import numpy as np
from math import cos,sin,radians,degrees,atan2,sqrt

class quaternion:
    def __init__(self,w,x,y,z):
        self.w=float(w)
        self.x=float(x)
        self.y=float(y)
        self.z=float(z)
        
    def mag(self):
        return sqrt(self.w*self.w+self.x*self.x+self.y*self.y+self.z*self.z)
        
    def inverse(self):
        mag=self.mag()
        return quaternion(self.w/mag,-self.x/mag,-self.y/mag,-self.z/mag)
        
    def mul(self,q):
        w=self.w*q.w - self.x*q.x - self.y*q.y - self.z*q.z
        x=self.w*q.x + self.x*q.w + self.y*q.z - self.z*q.y
        y=self.w*q.y - self.x*q.z + self.y*q.w + self.z*q.x
        z=self.w*q.z + self.x*q.y - self.y*q.x + self.z*q.w
        return quaternion(w,x,y,z)
        
    def printq(self):
        print self.w, self.x, self.y, self.z
        

def rotateVector(vec, axis, angle):

    p = quaternion(0,vec[0],vec[1],vec[2])
    q = quaternion(cos(angle/2),axis[0]*sin(angle/2),axis[1]*sin(angle/2),axis[2]*sin(angle/2))
    resq = q.mul(p.mul(q.inverse()))
    
    return np.array([resq.x,resq.y,resq.z])
    
def rotateVectorAboutPoint(vec, axis, centerOfRotation, angle):

    vecLoc = np.array(vec) - np.array(centerOfRotation)

    p = quaternion(0,vecloc[0],vecloc[1],vecloc[2])
    q = quaternion(cos(angle/2),axis[0]*sin(angle/2),axis[1]*sin(angle/2),axis[2]*sin(angle/2))
    resq = q.mul(p.mul(q.inverse()))
    
    rotVecLoc = np.array([resq.x,resq.y,resq.z])
    
    return  centerOfRotation + rotVec
    
    
#~ q1 = quaternion(0.5,0.5,0.5,0.5)
#~ 
#~ q1.printq()
#~ 
#~ print q1.mag()
#~ 
#~ q2 = q1.inverse()
#~ 
#~ q2.printq()
#~ 
#~ print q2.mag()

