#! /bin/python
# backupMesh.py
# does the backup of the mesh

import subprocess as sp
from os.path import join
import os
import argparse

def main(case,suffix):

    mesh1 = join(case,"constant/polyMesh"+"_"+suffix)
    mesh2 = join(case,"constant/polyMesh")
    
    sp.call(["rm","-r",mesh2])
    sp.call(["cp","-r",mesh1,mesh2])

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument('-s', dest='suffix', help="Which suffix does the backup mesh have?")
    parser.set_defaults(suffix="bak")
    args = parser.parse_args()

    main(os.getcwd(),args.suffix)
