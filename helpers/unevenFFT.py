from numpy import sin, linspace, pi
from pylab import plot, show, title, xlabel, ylabel, subplot
from scipy import fft, arange
import matplotlib.pyplot as plt
import numpy as np
import scipy.signal as signal

def plotSpectrum(y,Fs):
    """
    Plots a Single-Sided Amplitude Spectrum of y(t)
    """
    n = len(y) # length of the signal
    k = arange(n)
    T = n/Fs
    frq = k/T # two sides frequency range
    frq = frq[range(n/2)] # one side frequency range

    Y = fft(y)/n # fft computing and normalization
    Y = Y[range(n/2)]
 
    plot(frq,abs(Y),'r') # plotting the spectrum
    xlabel('Freq (Hz)')
    ylabel('|Y(freq)|')


x = np.sort(np.random.rand(1000))*pi*10
#print x

#First define some input parameters for the signal:
 
A = 2.
w = 1.
phi = 0.5 * np.pi
nin = 1000
nout = 3000
frac_points = 0.9 # Fraction of points to select

#Randomly select a fraction of an array with timesteps:
 

#r = np.random.rand(nin)
#x = np.linspace(0.01, 10*np.pi, nin)
#x = x[r >= frac_points]

normval = x.shape[0] # For normalization of the periodogram

#Plot a sine wave for the selected times:
 
y = A * np.sin(w*x+phi) +  1.5 * A * np.sin((w+2)*x+phi) + 3.0 * A * np.sin((w+5)*x+phi)

#Define the array of frequencies for which to compute the periodogram:
 
f = np.linspace(0.01, 10, nout)

#Calculate Lomb-Scargle periodogram:
 
pgram = signal.lombscargle(x, y, f)

#Now make a plot of the input data:
 

plt.subplot(2, 1, 1)
plt.plot(x, y, 'b')

# Then plot the normalized periodogram:
 
plt.subplot(2, 1, 2)
plt.plot(f, np.sqrt(4*(pgram/normval)))
plt.show()


