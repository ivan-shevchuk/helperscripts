#! /bin/python
# cutWithPlane.py
# cowabunga

# Generates the cuttingPlane and writes its parameters into snappyHexMeshDict.
# And the cuts the mesh using snappy

import sys
import os
from math import *
from quaternion import *
import subprocess
import argparse

import numpy as np
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

defaultCutMeshDictPath = "system/cutMeshDict"

def main(caseInput,relDictPath=defaultCutMeshDictPath):

    case=caseInput
    print case
    print relDictPath

    cutMeshDictPath=os.path.join(case,relDictPath)
    cutMeshDict = ParsedParameterFile(cutMeshDictPath,noHeader=True)

    point1 = np.array(cutMeshDict["planePoint1"]) #np.array([0, 11.3375, -0.7]) #bottom/origin
    point2 = np.array(cutMeshDict["planePoint2"]) #np.array([0, 12.6125,  0  ]) #waterlevel

    i = [1, 0, 0]
    j = [0, 1, 0]
    k = [0, 0, 1]

    dif = point2-point1
    angle = atan(dif[2]/dif[1])

    print degrees(angle)

    krot = rotateVector(k,i,angle)

    print krot

    sHMDict = os.path.join(case,"system/snappyHexMeshDict")
    sHMTemplatePath=os.path.join(case,"system/snappyHexMeshDict.template")

    subprocess.call(["rm",sHMDict])
    subprocess.call(["cp",sHMTemplatePath,sHMDict])

    sHM      = ParsedParameterFile(sHMDict)
    geometry = sHM["geometry"]

    geometry["cuttingPlane"]["pointAndNormalDict"]["basePoint"]    = point1.tolist()
    geometry["cuttingPlane"]["pointAndNormalDict"]["normalVector"] = krot.tolist()
    sHM.writeFile()

    subprocess.call(["snappyHexMesh","-case",case])

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("--case", dest="case",help="Folder, containing the mesh to be cut",metavar="CASE",default=".")
    parser.add_argument("--dict", dest="dictPath",help="cutMeshDict path",metavar="PATH",default=defaultCutMeshDictPath)
    args = parser.parse_args()
    main(args.case,args.dictPath)
