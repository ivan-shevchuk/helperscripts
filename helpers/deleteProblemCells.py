#! /bin/python

# deleteProblemCells.py
# cowabunga

import subprocess as sp
import glob as gb
import os,sys
import cutMesh

def main():

    sp.call(["checkMesh"])
    sp.call(["setSet","-batch","system/deleteProblemCells.batch"])
    sp.call(["subsetMesh", "cellsToDelete","-overwrite"])
    sp.call(["changeDictionary"])
    sp.call(["checkMesh"])

def additional():

    sp.call(["cp","-r","constant/polyMesh","constant/polyMesh_delcells"])


if __name__ == "__main__":
    main()
    additional()
