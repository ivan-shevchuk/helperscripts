#! /bin/python
# extrudeMesh.py
# extrudes the boundaries

import subprocess as sp
import glob as gb
import os,sys
import cutMesh
from os.path import join
import deleteProblemCells

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

def main(case,boundariesList):

    for boundary in boundariesList:
        print boundary
        
        extrMeshDictPath = join(case,"system/extrudeMeshDict");
        sp.call(["cp",extrMeshDictPath+"_"+boundary,extrMeshDictPath])
        
        extrudeMesh = ParsedParameterFile(extrMeshDictPath)
        extrudeMesh["sourceCase"] = case 
        
        extrudeMesh.writeFile()
        
        sp.call(["extrudeMesh","-case",case])
    
    sp.call(["foamToVTK","-case",case])
    additional(case)

def additional(case):

    mesh1 = join(case,"constant/polyMesh")
    mesh2 = join(case,"constant/polyMesh_extruded")
    sp.call(["cp","-r",mesh1,mesh2])

if __name__ == "__main__":

    main(os.getcwd(),["inlet","outlet"])
    sp.Popen(["paraview"])
