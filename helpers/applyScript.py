#! /bin/python

# createMeshesPar.py

# creates a set of openfoam meshes from starccm+ format and cuts the seabed
# developed especially for BAW project.
# can be easily modifed for any other case

import sys,os,shutil,multiprocessing
from math import *
from quaternion import *
from argparse import ArgumentParser
import numpy as np
import createMesh 
import itertools
from subprocess import call
from os.path import join

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

# THE WORST SHITCODE YOU'VE EVER SEEN
# DON'T JUDGE TOO STRICTLY, IT IS MY FIRST ATTEMPT OF PARALLELIZING IN PYTHON



def main(destinationPath,templatePath,meshesPath):


    

def createMeshInFolder(par):

    #ParameterList = zip(depthShortcuts,depths,listDP,listTP,listMP)
    depthShortcut       = par.depthShortcut
    depth               = par.depth 
    Bws                 = par.Bws
    Bs                  = par.Bs
    destinationPath     = par.destinationPath
    templatePath        = par.templatePath
    meshesPath          = par.meshesPath
    
    dirPath = os.path.join(destinationPath, depthShortcut)
    
    if os.path.exists(dirPath): 
        shutil.rmtree(dirPath)
 
    shutil.copytree(templatePath,dirPath)
    print "Copied template mesh to folder" , os.path.abspath(dirPath)

    cutMeshDictPath = os.path.join(dirPath,"system/cutMeshDict")
    
    if not os.path.exists(cutMeshDictPath):
       open(cutMeshDictPath,'w').close()
       
    cutMeshDict = ParsedParameterFile(cutMeshDictPath,noHeader=True)   
    
    cutMeshDict["cuttingType"] = "plane"
    cutMeshDict["planePoint1"] = [ 0.0, Bs , -depth] #bottom
    cutMeshDict["planePoint2"] = [ 0.0, Bws, 0]
    
    cutMeshDict["splittingParameters"] = dict([])
    cutMeshDict["splittingParameters"]["domainBox"] = dict([])
    
    cutMeshDict["splittingParameters"]["domainBox"]["p1"] = [-10, 0.0  , -1.0]
    cutMeshDict["splittingParameters"]["domainBox"]["p2"] = [ 20, 13.0 ,  1.5]
    cutMeshDict["splittingParameters"]["ySplit"] = 10
    
    cutMeshDict.writeFile()
    
    call(["cp",join(meshesPath,depthShortcut)+".ccm",dirPath])
    createMesh.main(dirPath,join(meshesPath,depthShortcut)+".ccm")

if __name__ == "__main__":

    parser = ArgumentParser()
    
    parser.add_argument("-command","--template",dest="template",help="Folder, containing template case",metavar="TEMPL",default=".")

    
    args  = parser.parse_args()
    
    templatePath       = args.template
    destinationPath    = args.output
    meshesPath         = args.mesh
    
    main(destinationPath,templatePath,meshesPath)

        

