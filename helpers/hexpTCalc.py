#! /bin/python

import os,sys

# running the script
# python hexpTCalc.py 0.000025 14 1.25


fth = float(sys.argv[1]) # First layer thickness

n = int(sys.argv[2]) # number of layers in it

r = float(sys.argv[3]) # growth ratio

S = fth *(1- pow(r,n))/(1-r)

print S
