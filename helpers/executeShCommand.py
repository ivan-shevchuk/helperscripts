from argparse import ArgumentParser
import copy
from subprocess import call,check_call

from math import *

def main(commandToExecute,casePath="./"):

    completeCommand = "( cd "+casePath + " ; " + commandToExecute + " )"
    print "Executing ", completeCommand , " in " , casePath
    check_call(completeCommand,shell=True)

if __name__ == "__main__":

    parser = ArgumentParser()
        
    parser.add_argument("-c","--case",dest="casePath",help="Case path",metavar="FILE",default="./")
    parser.add_argument("-e","--command",dest="command",help="Command to execute",default="ls")

    args  = parser.parse_args()
    
    casePath  = args.casePath
    command = args.command
    
    main(command,casePath)
