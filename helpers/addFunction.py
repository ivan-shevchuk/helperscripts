#! /bin/python
# adds specified function name to controlDict 

import sys,os

import numpy as np
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *


case = sys.argv[1]
functionName = sys.argv[2]

controlDictPath = os.path.join(case,"system/controlDict")
functionPath = os.path.join(case,"system/functions/"+functionName)

controlDict     = ParsedParameterFile(controlDictPath,doMacroExpansion=False)
#functions        = ParsedParameterFile(function

functions=controlDict["functions"]

print functions

entry="#include \"functions/"+functionName +"\"\n"

print "Searching for "+ entry + " in controlDict"

if  entry in functions.values():
    print "Entry already exists in the dictionary"
else:
    print "Adding new function to the controlDict:" + functionName
    n=len(functions)
    functions[n+1]=entry
    
    
controlDict.writeFile()
 



