#! /bin/python

import os,sys

# running the script
# python starYCalc.py 0.2 14 1.25


S = float(sys.argv[1]) # Total thickness of BL mesh
n = int(sys.argv[2]) # number of layers in it
r = float(sys.argv[3]) # growth ratio

qTimes2 = S*(r-1)/(pow(r,n)-1) # here the thickness of the cell is calculated

q=qTimes2/2 # this is the real y value - half of the cell thickness

print q
