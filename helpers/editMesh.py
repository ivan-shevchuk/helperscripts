
#! /bin/python
# editMesh.py
# cowabunga

import subprocess as sp
import glob as gb
import os,sys
from os.path import join
import quaternion
import cutMesh as cut
import deleteProblemCells as delete
import extrudeMesh as extrude
import argparse
import quaternion

def main(case=os.getcwd(),doCutMesh=False,doExtrudeMesh=False,doFixMesh=False):

    rm1List = gb.glob(join(case,"0/cell*"))
    rm2List = gb.glob(join(case,"0/*"))
    sp.call(["rm"]+rm1List+rm2List)

    cpList = gb.glob(join(case,"0_org/*"))
    sp.call(["cp"]+cpList+["0/"])

    if doCutMesh:
    
       cut.main(case)

    if doFixMesh:
    # has not been tested
        delete.main()

    if doExtrudeMesh:

        extrude.main(case,["inlet","outlet"])

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument('--case', dest='casedir', help="Do mesh extrude?")

    parser.add_argument('--do-cut', dest='docut', help="Do a mesh cutting?")
    parser.add_argument('--do-not-cut', dest='docut')

    parser.add_argument('--do-extrude', dest='doextr', help="Do mesh extrude?")
    parser.add_argument('--do-not-extrude', dest='doextr')

    parser.add_argument('--do-fix', dest='dofix', help="Delete problem cells?")
    parser.add_argument('--do-not-fix', dest='dofix' )

    parser.set_defaults(docut=False,dofix=False,doextr=False)
    parser.set_defaults(casedir=os.getcwd())

    args = parser.parse_args()

    main(arg.casedir,args.docut,args.doextr,args.dofix)
