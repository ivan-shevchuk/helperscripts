#! /bin/python

# createMeshesPar.py

# creates a set of openfoam meshes from starccm+ format and cuts the seabed
# developed especially for BAW project.
# can be easily modifed for any other case

import sys,os,shutil,multiprocessing
from math import *
from quaternion import *
from argparse import ArgumentParser
import numpy as np
import createMesh 
import itertools
from subprocess import call
from os.path import join

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

# THE WORST SHITCODE YOU'VE EVER SEEN
# DON'T JUDGE TOO STRICTLY, IT IS MY FIRST ATTEMPT OF PARALLELIZING IN PYTHON

class channelParameters:
    def __init__(self,depthShort,depth,Bws,Bs,destinationPath,templatePath,meshesPath):
     #   self.T = T
        self.depthShortcut = depthShort
        self.depth = depth
        self.Bws = Bws
        self.Bs = Bs
        self.destinationPath = destinationPath
        self.templatePath = templatePath
        self.meshesPath =  meshesPath
    def printParam(self):
        print self.depthShortcut, self.depth, self.Bws, self.Bw, self.destinationPath, self.templatePath,self.meshesPath

def main(destinationPath,templatePath,meshesPath):

    depthShortcuts       = ["1.06T","1.15T","1.2T","1.3T","1.5T","1.75T"]
    depthToDraftRatios   = [1.0625,1.15,1.2,1.3,1.5,1.75]
    T                    = 0.4
    depths = map(lambda a: a*T, depthToDraftRatios)
    
    #T                    = [0.4]            * len(depthShortcuts)
    Bws                  = [12.6125]        * len(depthShortcuts)
    Bs                   = [11.3375]        * len(depthShortcuts)
    listDP               = [destinationPath]* len(depthShortcuts) 
    listTP               = [templatePath]   * len(depthShortcuts)
    listMP               = [meshesPath]       * len(depthShortcuts)
    
    zipAll=zip(depthShortcuts,depths,Bws,Bs,listDP,listTP,listMP) 
    inputParam = itertools.starmap( channelParameters , zipAll)
    
    #print list(inputParam)
    if not os.path.exists(destinationPath):
        os.makedirs(destinationPath)
    
    pool=multiprocessing.Pool(4)
    pool.map(createMeshInFolder,inputParam)
    

def createMeshInFolder(par):

    #ParameterList = zip(depthShortcuts,depths,listDP,listTP,listMP)
    depthShortcut       = par.depthShortcut
    depth               = par.depth 
    Bws                 = par.Bws
    Bs                  = par.Bs
    destinationPath     = par.destinationPath
    templatePath        = par.templatePath
    meshesPath          = par.meshesPath
    
    dirPath = os.path.join(destinationPath, depthShortcut)
    
    if os.path.exists(dirPath): 
        shutil.rmtree(dirPath)
 
    shutil.copytree(templatePath,dirPath)
    print "Copied template mesh to folder" , os.path.abspath(dirPath)

    cutMeshDictPath = os.path.join(dirPath,"system/cutMeshDict")
    
    if not os.path.exists(cutMeshDictPath):
       open(cutMeshDictPath,'w').close()
       
    cutMeshDict = ParsedParameterFile(cutMeshDictPath,noHeader=True)   
    
    cutMeshDict["cuttingType"] = "plane"
    cutMeshDict["planePoint1"] = [ 0.0, Bs , -depth] #bottom
    cutMeshDict["planePoint2"] = [ 0.0, Bws, 0]
    
    cutMeshDict["splittingParameters"] = dict([])
    cutMeshDict["splittingParameters"]["domainBox"] = dict([])
    
    cutMeshDict["splittingParameters"]["domainBox"]["p1"] = [-10, 0.0  , -1.0]
    cutMeshDict["splittingParameters"]["domainBox"]["p2"] = [ 20, 13.0 ,  1.5]
    cutMeshDict["splittingParameters"]["ySplit"] = 10
    
    cutMeshDict.writeFile()
    
    call(["cp",join(meshesPath,depthShortcut)+".ccm",dirPath])
    createMesh.main(dirPath,join(meshesPath,depthShortcut)+".ccm")

if __name__ == "__main__":

    parser = ArgumentParser()
    
    parser.add_argument("-t","--template",dest="template",help="Folder, containing template case",metavar="TEMPL",default=".")
    parser.add_argument("-m","--mesh",dest="mesh",help="Folder, containing the mesh for different depths",metavar="MESH",default="./meshes/")
    parser.add_argument("-d","--output-folder",dest="output",help="Folder for writing generated cases",default="./cases")
    
    args  = parser.parse_args()
    
    templatePath       = args.template
    destinationPath    = args.output
    meshesPath         = args.mesh
    
    main(destinationPath,templatePath,meshesPath)

        

