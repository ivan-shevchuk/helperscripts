#! /bin/python

import sys
import os
from os.path import join
from math import *
from quaternion import *
import argparse
from subprocess import call

import numpy as np
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *


dictReplacementName ="dictionaryReplacement"

def main(quantityName,boundaryName,newBCName,case=os.getcwd(),time="0"):

    scriptTemplPath = os.getenv("SCRIPT_TEMPL")
    #print scriptTemplPath
    
    changeDictTemplPath = join(scriptTemplPath,"changeDictionaryTemp/changeDictionaryDict.template")
    #print changeDictTemplPath
    
    relCDDPath = "system/changeDictionaryDict_"+quantityName
    destDictPath = join(case,relCDDPath)
    
    call(["cp",changeDictTemplPath,destDictPath])
    
    changeDict = ParsedParameterFile(destDictPath)
    
    changeDict[dictReplacementName][quantityName]=dict([])
    changeDict[dictReplacementName][quantityName]["boundaryField"]=dict([])
    changeDict[dictReplacementName][quantityName]["boundaryField"][boundaryName]=dict([])
    changeDict[dictReplacementName][quantityName]["boundaryField"][boundaryName]["type"]=newBCName
    
    changeDict.writeFile()
    #print changeDict
    
    call(["changeDictionary","-case",case,"-dict",relCDDPath,"-time",time])
    

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument("--case",dest="case",help="Case path",metavar="PATH",default=os.getcwd())
    parser.add_argument("-b","--boundary",dest="boundary",help="Boundary name",metavar="NAME",default="inlet")
    parser.add_argument("-q","--quantity",dest="quantity",help="Quatity to change BC for",default="alpha.water")
    parser.add_argument("--bctype",dest="bctype",help="New BC type for specified quantity",default="fixedValue")
    parser.add_argument("--time",dest="time",help="Time",default="0")
    
    args  = parser.parse_args()
    
    case        = args.case
    boundary    = args.boundary
    quantity    = args.quantity
    newbc       = args.bctype
    time        = args.time
    
    main(quantity,boundary,newbc,case,time)
