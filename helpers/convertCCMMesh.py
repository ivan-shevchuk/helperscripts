#! /bin/python

# convertCCMMesh.py
# cowabunga
from subprocess import call
import os
import glob as gb
import sys
import argparse
from os.path import join

def main(case=os.getcwd(),meshFileName=os.path.basename(os.getcwd())+".ccm",doBackupMesh=True):

    call(["ccm26ToFoam",meshFileName,"-case",case])
    rm1List = gb.glob(join(case,"0/cell*"))
    
    call(["rm","-r"]+rm1List)
    
    if doBackupMesh:
        backupMesh(case)

def backupMesh(case=os.getcwd()):
    call(["cp","-r",os.path.join(case,"constant/polyMesh"),os.path.join(case,"constant/polyMesh_init")])
    
if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument('--ccm', dest='ccmmesh', help="Mesh to convert")
    parser.add_argument('--case', dest='casedir', help="Do mesh extrude?")

    parser.set_defaults(ccmmesh=os.path.basename(os.getcwd())+".ccm",casedir=".")

    args = parser.parse_args()

    try:
        main(args.casedir,args.ccmmesh)
    except:
        print "Error occured during mesh conversion. Please check the parameters"

        

