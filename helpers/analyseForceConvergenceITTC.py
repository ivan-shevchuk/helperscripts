#! /bin/sh


import sys,os,shutil,re
from math import *
from argparse import ArgumentParser
from os.path import join
import copy

from math import *
from vtk import *
import numpy as np

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages

import commonVariables as comv
from commonVariables import tex,isfloat,float_cmp_toint,cmp_foam_times,getLatestFoamTime,getLatestFileTime

def readLastNForcesDat(casePath,entity,timeInterval,columns):

    inputFolder = join(casePath,comv.postProcessingPath,entity)
    outputFolder = join(casePath,comv.plottingPath,entity)
    
    print outputFolder
    
    if os.path.exists(outputFolder):
        shutil.rmtree(outputFolder)

    os.makedirs(outputFolder)
    # a folder, where the filtered concatenated forces are

    times=filter(isfloat, os.listdir(inputFolder))
    times.sort(cmp_foam_times)
    print times
    
    filenames=[]
    for time in times:
        filenames.append(join(inputFolder,str(time),entity+".dat"))
    
    
    latestTime = getLatestFileTime(filenames[-1])
    timeInterval = [latestTime-timeInterval,latestTime]


    alllines = [];
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                alllines.append(line)
    
    iter=0
    lastConsistentTime=0.00000
    
    conti_lines = []
    
    output=open(join(outputFolder,entity+".dat"),'w')

    for line in alllines:
      ## keep the head of the file
      if(iter>=4):
        newLines = line.replace("("," ")
        newLines = newLines.replace( ")" , " ")
        newLinesSplit = newLines.split();

        if(newLinesSplit[0]!="#"):
          ## ensure that the plots will be continuous (no jumps in time)
          lineStart = float(newLinesSplit[0])
          if ( lineStart > lastConsistentTime and lineStart >= timeInterval[0] and lineStart <= timeInterval[1]):
             if entity=="forces":             
                 conti_lines.append(map(float,[newLinesSplit[i] for i in columns]))
             #~ elif entity=="forceCoeffs": 
                 #~ conti_lines.append(map(float,[newLinesSplit[0],newLinesSplit[11],newLinesSplit[14]]))
                
             output.write(newLines)
             lastConsistentTime=float(newLinesSplit[0])
      iter+=1
      
    output.close()
    return conti_lines

# Calculate variations of solution between the
# meshes
def calcVariation(vec):
    
    variations=[]
    for i in range(0,len(vec)-1):
      variations.append(vec[i+1]-vec[i])
    return variations
      
# Convergence ratio
# 0 < R < 1 - monotonic convergence
# R < 0 - oscillatory convergence/divergence
# R > 1 - divergence
def calcR(variations,which):

    if which == 0:
       R = variations[1]/variations[0]
    elif which == 1:
       R = variations[2]/variations[1]

    return R
    
def calcUncertainty(varVec):
# designations in ittc: 3-coarse, 2-medium, 1-fine

    Ucorr = 0
    U = 0
    pTheor = 2
    p = -1
    result = ""

    variation = calcVariation(varVec) 
    # eps21 / eps32
    R = variation[2]/variation[1]
    
    if (R > 0 and R < 1):
    # monotonic convergence
        eps21 = variation[2]
        
        print eps21
        
        p = np.log(1.0/R)/np.log(np.sqrt(2))
        
        print p
        
        C = (np.power(np.sqrt(2),p) - 1) / (np.power(np.sqrt(2),pTheor) - 1)
        
        print C
        
        deltaRe = C*(eps21 / (np.power(np.sqrt(2),p) - 1))
        
        if (np.abs(1-C) >= 0.125):
            U = (2*np.abs(1-C)+1)*np.abs(deltaRe)
        elif (np.abs(1-C) < 0.125): 
            U = (9.6*(1-C)*(1-C) + 1.1)*np.abs(deltaRe)
            
        if (np.abs(1-C) >= 0.25):
            Ucorr = np.abs(1-C)*np.abs(deltaRe)
        elif (np.abs(1-C) < 0.25): 
            Ucorr = (2.4*(1-C)*(1-C) + 0.1)*np.abs(deltaRe)
           
        U = U*1.25    
        Ucorr = Ucorr*1.25 
        
        result = "CONV"
        
    elif (R < 0):
    # oscillatory convergence
        U = Ucorr = 0.5*(max(varVec) - min(varVec))
        U = U*1.25    
        Ucorr = Ucorr*1.25
        
        result = "OCONV"
    
    elif (R > 1):
    # divergence
        U = Ucorr = (max(varVec) - min(varVec))
        U = U*3  
        Ucorr = Ucorr*3
        result = "DIV"
        
    return result, R, p, U, Ucorr, variation
    

def main(configFile,casePath="./"):

    config = eval(open(configFile,"r").read());

    configName = configFile.split("/")[-1]
    configName = configName.split(".")[0]
    print "Configuration: ", configName
    
    cases = config["cases"]
    triples = config["triples"]
    columns = config["columns"]
    interval = config["interval"]
    halfship = config["halfship"]
    
    analysedPaths = cases

    rhoRef = 1000
    
    wfilePath = os.path.join(casePath,"convergence-analysis-"+configName);
    wfile = open(wfilePath,'w')
    
    j = 0
    for triple in triples:
       
        var1Vec=[]
        var2Vec=[]
        var3Vec=[]
        
        print triple
    
        for path in analysedPaths:
        
            caseParameters = ParsedParameterFile(join(casePath,path,comv.caseParametersFile),noHeader=False)
            lRef = caseParameters["lRef"]
            ARef = caseParameters["ARef"]
            Uinf = caseParameters["Uinf"]
    
            timeInterval = interval 
            print [0,columns[j][0],columns[j][1]]
            
            lines = np.array(readLastNForcesDat(join(casePath,path),"forces",timeInterval,[0,columns[j][0],columns[j][1]]))
            linesT = lines.T
    
            t = linesT[0]
            
            var1 = (linesT)[1]
            var2 = (linesT)[2]
            var3 = np.add(var1,var2)
            
            halfCoeff = 1
            if halfship == True:
                halfCoeff = 2
            
            var1M = halfCoeff*var1.mean()
            var2M = halfCoeff*var2.mean()
            var3M = halfCoeff*var3.mean()
            
            if triple[0][0] == "C":
                var1M = var1M / (0.5 * 1000 * Uinf * Uinf * ARef )
                var2M = var2M / (0.5 * 1000 * Uinf * Uinf * ARef )
                var3M = var3M / (0.5 * 1000 * Uinf * Uinf * ARef )
                
            elif triple[0][0] == "m":
                var1M = var1M / (0.5 * 1000 * Uinf * Uinf * ARef * lRef )
                var2M = var2M / (0.5 * 1000 * Uinf * Uinf * ARef * lRef )
                var3M = var3M / (0.5 * 1000 * Uinf * Uinf * ARef * lRef )
            
            var1Vec.append(var1M)
            var2Vec.append(var2M)
            var3Vec.append(var3M)
          
        print var1Vec,var2Vec,var3Vec
    
        print triple[0]," grid convergence par-s: "
        resVar1, Rvar1, pvar1, Uvar1, Ucorrvar1, var1Var= calcUncertainty(var1Vec)
        print resVar1, ", R = " , Rvar1, ", p = ", pvar1, ", U =", Uvar1, ", Ucorr =" , Ucorrvar1

        print triple[1]," grid convergence par-s: "
        resVar2, Rvar2, pvar2, Uvar2, Ucorrvar2, var2Var = calcUncertainty(var2Vec)
        print resVar2, ", R = " , Rvar2, ", p = ", pvar2, ", U =", Uvar2, ", Ucorr =" , Ucorrvar2

        print triple[2]," grid convergence par-s: "
        resVar3, Rvar3, pvar3, Uvar3, Ucorrvar3, var3Var= calcUncertainty(var3Vec)
        print resVar3, ", R = " , Rvar3, ", p = ", pvar3, ", U =", Uvar3, ", Ucorr =" , Ucorrvar3


        wfile.write('# mesh \t index \t ' + triple[0] + '[-] \t eps \t ' + triple[1] + '[-] \t eps \t '+ triple[2] + '[-] \t eps \n')
        for i in range(0,len(analysedPaths)):
    
             var1v = 0
             var2v = 0
             var3v = 0
             if i > 0:
                 var1v = var1Var[i-1]
                 var2v = var2Var[i-1]
                 var3v = var3Var[i-1]
    
             output =  analysedPaths[i] + '\t' + str(i) + '\t' + str(var1Vec[i]) + '\t' + str(var1v) + '\t'\
                       + str(var2Vec[i]) + '\t' + str(var2v) + '\t' + str(var3Vec[i])  + '\t' + str(var3v) + '\n'
             wfile.write(output)
        
        wfile.write('R \t - \t ' + str(Rvar1) + ' \t - \t ' + str(Rvar2) + '\t - \t '+ str(Rvar3) + '\t - \n')
        wfile.write('res \t - \t ' + resVar1 + ' \t - \t ' + resVar2 + '\t - \t '+ resVar3 + '\t - \n')
        wfile.write('p \t - \t ' + str(pvar1) + ' \t - \t ' + str(pvar2) + '\t - \t '+ str(pvar3) + '\t - \n')
        wfile.write('U \t - \t ' + str(Uvar1) + ' \t - \t ' + str(Uvar2) + '\t - \t '+ str(Uvar3) + '\t - \n')
        # relative uncertainty (in percent of the finest mesh solution
        wfile.write('U-rel \t - \t ' + str(abs(Uvar1/var1Vec[-1])*100) + ' \t - \t ' + str(abs(Uvar2/var2Vec[-1])*100) \
             + '\t - \t '+ str(abs(Uvar3/var3Vec[-1])*100) + '\t - \n')
        wfile.write('Ucorr \t - \t ' + str(Ucorrvar1) + ' \t - \t ' + str(Ucorrvar2) + '\t - \t '+ str(Ucorrvar3) + '\t - \n')
        # relative uncertainty (in percent of the finest mesh solution
        wfile.write('Ucorr-rel \t - \t ' + str(abs(Ucorrvar1/var1Vec[-1])*100) + ' \t - \t ' + str(abs(Ucorrvar2/var2Vec[-1])*100) \
             + '\t - \t '+ str(abs(Ucorrvar3/var3Vec[-1])*100) + '\t - \n')
    
        wfile.write("\n\n")

        j = j+1
    
    wfile.close()
    os.system("libreoffice5.1 --calc "+ wfilePath + " &")
    



if __name__ == "__main__":

    parser = ArgumentParser()
        
    parser.add_argument("-c","--case",dest="casePath",help="Case path",metavar="FILE",default="./")
    
    parser.add_argument("-f","--conf-file",dest="configFile",help="File with description of configurations")
    args  = parser.parse_args()
    configFile = os.path.abspath(args.configFile)

    casePath  = args.casePath
    
    main(configFile,casePath)
