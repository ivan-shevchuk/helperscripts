import sys,os,shutil,re
from math import *
from os.path import join
import copy

import numpy as np

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages

# functions and variables, common for all cases (like  postProcessing, forceCoeffs keywords)
import commonVariables as comv
from commonVariables import *

# TEST for convertVarNameToTex
title = convertVarNameToTex("Ux")
print title
a = [1, 2, 3, 4]
b = [1, 4, 9, 16]
comv.setTexStyle()
ax, = plt.plot(a,b)
plt.legend([ax],[title])
plt.show()

plt.clf()

#~ # TEST for convertSetNameToTex
title = convertSetNameToTex("x=0p5")
print title
a = [1, 2, 3, 4]
b = [1, 4, 9, 16]
comv.setTexStyle()
ax, = plt.plot(a,b)
plt.legend([ax],[title])
plt.show()
