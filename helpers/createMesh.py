#! /bin/python

# createMesh.py
# cowabunga

import cleanMesh as clear
import convertCCMMesh as convert
import editMesh as edit
import pvopenstate as pvos
import subprocess as sp
import os
from os.path import join

def main(case=os.getcwd(),meshPath=join(os.getcwd(),os.path.basename(os.getcwd())+".ccm")):

    print case
    clear.main(case)
    convert.main(case, meshPath)
    edit.main(case,doCutMesh=True,doExtrudeMesh=True)

    sp.call(["changeDictionary","-case",case,"-dict",join(case,"system/changeDictionaryDict_boundary")])
    sp.call(["checkMesh","-case",case])
    sp.call(["foamToVTK","-case",case])
    #pvos.main(case,join(case,"checkBadCells.pvsm"))

if __name__ == "__main__":

    
    main()
