#! /bin/python
# containts scripts common variables, should be included everywhere
# test can be found in tests folder

import sys,os
import matplotlib.pyplot as plt
import matplotlib
import subprocess
from os.path import join
import itertools
import glob
import json
from pprint import pprint

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.DataStructures import *

# Sets default style for fancy tex fonts and formulae support
def setTexStyle(): 
    font = {'family':'serif','serif':'Computer Modern Roman','size':16}
    matplotlib.rc('font',**font)
    matplotlib.rc('text',usetex=True)

def getLatestFoamTimeVar2(folder):
    command = "foamListTimes -case " + folder
    p = subprocess.Popen(command,shell=True,stdout=subprocess.PIPE)
    out, err = p.communicate()
    # filters \n out of the shell output   
    outSplit = out.split()      
    # get the last time
    latestTime = outSplit[-1]
    return latestTime
   
   
def iffloat(value):
    try:
        float(value)
        return float(value)
    except ValueError:
        return value

def removeChars(string,chars,replaceWith=" "):
    for ch in chars:
        if ch in string:
            string=string.replace(ch,replaceWith)
    return string

def getLatestFoamTime(folder):

    times=filter(isfloat, os.listdir(folder))
    times.sort(cmp_foam_times)
    return times[-1]

# All columns contain float data
def readFloatDataFile(fileName,columns):

    inputFile = open(fileName,'r')
    #print columns
    readLines = []
    for line in inputFile:
        if (line[0] != "#"):
            newline = removeChars(line,"()"," ")
            extracted_columns = [float(newline.split()[i-1]) for i in columns]
            readLines.append(extracted_columns)
            
    return readLines

# Some columns contain labels
def readMixedDataFile(fileName,columns):

    inputFile = open(fileName,'r')
    #print columns
    readLines = []
    for line in inputFile:
        if (line[0] != "#"):
            newline = removeChars(line,"()"," ")
            extracted_columns = [iffloat(newline.split()[i-1]) for i in columns]
            readLines.append(extracted_columns)
            
    return readLines

# Adds $ $ to the entry
def tex(entry):
    return ('$'+entry+'$').encode('string-escape')

def isfloat(x):
 try:
  float(x)
  return True;
 except:
  return False;

def float_cmp_toint(x):
  if x>0:
    return 1
  elif x<0:
    return -1
  elif x==0:
    return 0

def cmp_foam_times(t1,t2):
    return float_cmp_toint(float(t1)-float(t2))
    
varNamesTex={\
"magUMean":r"$|\overline{\mathbf{u}}|$",\
"UMean":r"$\overline{\mathbf{u}}$",\
"UMeanx":r"$\overline{u}_x$",\
"UMeany":r"$\overline{u}_y$",\
"UMeanz":r"$\overline{u}_z$",\
"magU":r"$|\mathbf{u}|$",\
"Ux":r"$u_x$",\
"Uy":r"$u_y$",\
"Uz":r"$u_z$",\
"UPrime2Meanxx":r"$\left< u^{\prime 2}_x \right>$",\
"UPrime2Meanyy":r"$\left< u^{\prime 2}_y \right>$",\
"UPrime2Meanzz":r"$\left< u^{\prime 2}_z \right>$",\
"UPrime2Meanxy":r"$\left< u'_x u'_y \right> $",\
"p":r"$p$",\
"k":r"$k$",\
"w":r"$w$"}

def convertVarNameToTex(name):

    texName=""
    try:
        texName=varNamesTex[name]
    except:
        texName=name
        
    return texName

def convertSetNameToTex(name):
     
    texName = name
    texName = texName.replace("p",".")
    texName = r"$"+texName+"$"
    return texName
    
def findSampleDicts(casePath):

    sampleDictsPath=os.path.join(os.path.abspath(casePath),"system")
    result = glob.glob(sampleDictsPath+"/sampleDict*")
    print result
    return result
    
def writeDict(dictt,destination):
    #~ with open(destination,'w') as out:
        #~ pprint(dictt, stream=out)   
    json.dump(dictt,open(destination,'w'),indent=2)
        
def readDict(filee):
    return json.load(open(filee,"r"))
    
def findAllSetsAndSurfaces(sampleDictsList):

    fieldsAll=[]
    setsAll=[]
    
    surfAll=[]
    basePoints=[]
    normVecs=[]
    
    setsFileNamesAll=[]
    surfFileNamesAll=[]

    for sd in sampleDictsList:
    
       sd = ParsedParameterFile(os.path.abspath(sd),noHeader=False,doMacroExpansion=True)
       
       setFormat=sd["setFormat"]
       surfaceFormat=sd["surfaceFormat"]
       
       varlist=sd["fields"]
       sets=convertFoamDictToPython(sd["sets"])
       surfaces=convertFoamDictToPython(sd["surfaces"])
       
       setExt = getExtensionByFomat(setFormat)
       surfExt = getExtensionByFomat(surfaceFormat)
       
       setsFileNames=generateSetFileNames(varlist,sets,setExt)
       surfFileNames=generateSetFileNames(varlist,surfaces,surfExt)
       
       fieldsAll=fieldsAll+varlist
       setsAll=setsAll+sets.keys()
       surfAll=surfAll+surfaces.keys()
    
       #print surfaces
       for surf in surfaces:
          if surfaces[surf]["type"]=="cuttingPlane":
          
              #print surfaces[surf]
              bp = surfaces[surf]["pointAndNormalDict"]["basePoint"]
              # gives PyFoam.Basics.DataStructures.Vector
              basePoints.append([bp[0],bp[1],bp[2]])
              normVec = surfaces[surf]["pointAndNormalDict"]["normalVector"]
              normVecs.append([normVec[0],normVec[1],normVec[2]])
          else:
              print "CAREFUL!! Surface ", surf," is not a cuttingPlane. NOT IMPLEMENTED!!" 
           
       setsFileNamesAll=setsFileNamesAll+setsFileNames
       surfFileNamesAll=surfFileNamesAll+surfFileNames
       
    #print basePoints
       
    return(fieldsAll,setsAll,surfAll,basePoints,normVecs,setsFileNamesAll,surfFileNamesAll)
    
# analogous to findAllSetsAndSurfaces, but writes all data into dicts
# and saves on disk
# so that the plotting script can use the corresponding information

def createNewFieldSubdict():
    return {"sets":{"name":[],"rangex":[],"type":[],"file":[],"plotSize":[]},\
    "surfaces":{"name":[],"basePoint":[],"normalVector":[],"rangex":[],"rangey":[],"plotSize":[],"file":[]}}
    
def _finditem(obj, key):
    if key in obj: return obj[key]
    for k, v in obj.items():
        if isinstance(v,dict):
            item = _finditem(v, key)
            if item is not None:
                return item
    
def collectDictValues(dictt,name):
    values = []
    for key in dictt.keys():
        values.append(_finditem(dictt[key],name))
    return values

    
def readPlottingConfig(sampleDictsList): 

    #format
    #{"UMean":{"sets":{"names":["set1","set2","set3"],"setsTypes":["..."],"rangex":[],"files":[],"plotSize":[]}\
    #,"surfaces":{"name":["surf1","surf"],"basePoint":[,,],"normalVector":[],"rangex":[],"rangey":[],"plotSize":[]}}}
    plottingConfig={}

    # first loop - collect all field names
    #print sampleDictsList
    
    #convertToVector = lambda x: [x[0],x[1],x[2]]
    convertToVector = lambda x: list(x)
    
    for sd in sampleDictsList:
    
        sdict = ParsedParameterFile(os.path.abspath(sd),noHeader=False,doMacroExpansion=True)
        
        setFormat=sdict["setFormat"]
        surfaceFormat=sdict["surfaceFormat"]
        
        setExt = getExtensionByFomat(setFormat)
        surfExt = getExtensionByFomat(surfaceFormat)
        
        varlist=sdict["fields"]
        sets=convertFoamDictToPython(sdict["sets"])
        surfaces=convertFoamDictToPython(sdict["surfaces"])
        
        for fieldName in varlist:
        
            if not fieldName in plottingConfig: 
                plottingConfig[fieldName] = createNewFieldSubdict()
                
            plottingConfig[fieldName]["sets"]["name"] = \
                 plottingConfig[fieldName]["sets"]["name"] + sets.keys()
                 
            plottingConfig[fieldName]["surfaces"]["name"] = \
                 plottingConfig[fieldName]["surfaces"]["name"] + surfaces.keys()
            
            for valName in ["type","rangex","plotSize"]:
                plottingConfig[fieldName]["sets"][valName]= \
                    plottingConfig[fieldName]["sets"][valName] + collectDictValues(sets,valName)
            
            for valName in ["basePoint","normalVector","rangex","rangey","plotSize"]:
                plottingConfig[fieldName]["surfaces"][valName]= \
                    plottingConfig[fieldName]["surfaces"][valName] + collectDictValues(surfaces,valName)

            plottingConfig[fieldName]["sets"]["file"] = plottingConfig[fieldName]["sets"]["file"] + \
            generateSetFileNames(varlist,sets,setExt)
           
            try: 
                plottingConfig[fieldName]["surfaces"]["basePoint"] = \
                map(convertToVector,plottingConfig[fieldName]["surfaces"]["basePoint"])
            except:
                print "Could not convert surface dicts to vector format"

            try:
                plottingConfig[fieldName]["surfaces"]["normalVector"] = \
                map(convertToVector,plottingConfig[fieldName]["surfaces"]["normalVector"])
            except:
                print "Could not convert surface dicts to vector format"

            plottingConfig[fieldName]["surfaces"]["file"] = plottingConfig[fieldName]["surfaces"]["file"] + \
            generateSetFileNames(varlist,surfaces,surfExt)

    return plottingConfig
    
def readCasePlottingConfig(casePath):

    sdList = findSampleDicts(casePath)
    plottingConfig = readPlottingConfig(sdList)
    writeDict(plottingConfig,os.path.join(casePath,"plottingConfig.txt"))
    
    return plottingConfig

def convertFoamDictToPython(foamDict):

    dictt={}
    
    i = 0
    while i < len(foamDict):
       key = foamDict[i].replace(" ","")
       dictt[key]=foamDict[i+1]
       i = i+2
    
    return dictt
    
def convertPythonDictToFoam(pythonDict):

    listt=[]
    
    for key,value in pythonDict.iteritems():
       listt.append(key)
       listt.append(value)
        
    return listt
      
def generateSetFileNames(varNames,setNames,extension):

    names=[]
    for varName in varNames:
        for setName in setNames:
            names.append(setName+"_"+varName+extension)
    return names

def generateSurfaceFileNames(varNames,setNames,extension):

    names=[]
    for varName in varNames:
        for setName in setNames:
            names.append(varName+"_"+setName+extension)
    return names
    
def getExtensionByFomat(f):
    rf = ""
    if f=="raw":
        rf="xy"
    else:
        rf=f
    rf="."+rf
    return rf

# 0 - x, y - 1, z - 2
# if n = ( 1 0 0 )
# the coordinate system in the cutting plane is: y - horizontal to the left
# z - vertical upwards
# if n = ( -1 0 0 )
# y - horizontal to the right
# z - vertical upwards
def getPlotAxesByNormal(n):
 
    hC = 0 # horizontal coord. x by default
    vC = 1 # vertical coord. y by default
    
    if (n == [1.0,0.0,0.0] or n == [-1.0,0.0,0.0]):
        print "x Section"
        hC = 1
        vC = 2
    elif (n == [0.0,1.0,0.0] or n == [0.0,-1.0,0.0]):
        print "y Section"
        hC = 0
        vC = 2
    elif (n == [0.0,0.0,1.0] or n == [0.0,0.0,-1.0]):
        print "z Section" # 
         #hC = 0 has already been set by default
    #    #vC = 1
    return (hC,vC)

def getAxisLabelByNumber(num):

    if (num != 0 and num != 1 and num != 2):
      print "getAxisLabelByNumber: Possibly wrong output generated"

    ret = "x"
    
    if num==1:
        ret="y"
    elif num==2:
        ret="z"
        
    return ret
    
       

    
def getSetsPath(casePath,time):
    return join(casePath,postProcessingPath,"sets",str(time))
    
def getTotalSetPath(casePath,time,setFileName):
    return join(casePath,postProcessingPath,"sets",str(time),setFileName)

def getSurfsPath(casePath,time):
    return join(casePath,postProcessingPath,"surfaces",str(time))
    

#Relative to the case root
plottingPath = "plottingStuff"

#Relative to plottingPath

waveElevationPath = "waveElevation"
forcesPath        = "forces"
forceCoeffPath    = "forceCoeff"

#Relative to the case root
postProcessingPath   = "postProcessing"
caseParametersFile   = "caseParameters"
shipPatch            = "ship"
shipMotionOutputFile = "motion"






 



