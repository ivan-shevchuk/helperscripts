#! /bin/python

# cleanMesh.py
# cowabunga

from subprocess import call
import os
from glob import glob

def main(case="."):

    path = os.path.join(case,"constant/polyMesh*")
    filesToRm = glob(os.path.join(case,"constant/polyMesh*"))
    if len(filesToRm):
       print "Removing: ", filesToRm
       call(["rm","-r"]+filesToRm)
    else:
       print "cleanMesh: No files to remove"

if __name__ == "__main__":
    main()
