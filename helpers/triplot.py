
from scipy.interpolate import griddata
import numpy as np
from vtk import *
from vtk.util.numpy_support import vtk_to_numpy

import matplotlib.pyplot as plt
import matplotlib 
from matplotlib import cm, colors, ticker
from matplotlib.backends.backend_pdf import PdfPages
from mpl_toolkits.axes_grid1 import make_axes_locatable

import subprocess
from os.path import join
from argparse import ArgumentParser

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile

import commonVariables as comv
from commonVariables import tex,isfloat,float_cmp_toint,cmp_foam_times

#~ def triPlot(fieldName,title,timeName,rangex,nx,rangey,ny,rangeScalar,size)
#~ fieldName - for example "magUMean"
#~ title - for example "ytoB=0"
#~ timeName - string - for reading proper file /postProcessing/surfaces/$time/magUMean_ytoB=0.vtk
#~ rangex - list [a,b] - for setting the range in x direction
#~ nx     - int - in the set of point data which of the coordinates is the x coordinate for plotting (0,1,2)?
#~ rangey - list [a,b] - for setting the range in y direction
#~ ny     - int - in the set of point data which of the coordinates is the y coordinate for plotting (0,1,2)?
#~ rangeScalar - clipping for plotted scalar
#~ size   - list [

def mainLambda(casePath,fieldName,fieldNameTex,title,titleTex,timeName,rangex,nx,rangey,ny,rangeField,size,lambdaFun):

    readfile = join(casePath,"analysis/surfaces",timeName,fieldName + "_" + \
         title + ".vtk")

    print "Reading file: ", readfile
    
    reader = vtk.vtkPolyDataReader()
    reader.SetFileName(readfile)
    reader.Update()
    output = reader.GetOutput()
    
    # Get the coordinates of nodes in the triangulated mesh
    nodes_vtk_array= output.GetPoints().GetData()
    # Get the scalar field to be plotted
    field_vtk_array = output.GetPointData().GetArray(fieldName)
    
    # Get cells (= triangles)
    polys = output.GetPolys()
    
    #Convert vtkPolys to [n1,n2,n3] - numpy format
    idList = vtk.vtkIdList()
    triangles=[]
    for i in range(polys.GetNumberOfCells()):
        polys.GetNextCell(idList)
        triangle = [idList.GetId(0),idList.GetId(1),idList.GetId(2)]
        triangles.append(triangle)
    
    #Get the coordinates of the nodes and their scalar values
    nodes_np = vtk_to_numpy(nodes_vtk_array)
   
    x,y,z= nodes_np[:,nx] , nodes_np[:,ny] , nodes_np[:,2]
    
    field_np_original = vtk_to_numpy(field_vtk_array)
    field_np = map(lambdaFun,field_np_original)
    ## how to apply lambda func to field np ??

    trianglesn = np.asarray(triangles)
    
    minF=rangeField[0]
    maxF=rangeField[1]
    
    fig = plt.figure(figsize=size,dpi=800)
    # cm = plt.cm.get_cmap('Diverging')
    font = {'family':'serif','serif':'Computer Modern Roman','size':25}
    matplotlib.rc('font',**font)
    matplotlib.rc('text',usetex=True) 

    ax = fig.add_subplot(111, aspect='auto')

    plt.xlim(rangex)
    plt.ylim(rangey)

    plt.title(titleTex)
    
    xlabel = ""
    
    if nx==0:
       xlabel = r"$x$ \ [m]"
    if nx==1:
       xlabel = r"$y$ \ [m]"
    if nx==2:
       xlabel = r"$z$ \ [m]"
    
    ylabel = ""
    
    if ny==0:
       ylabel = r"$x$ \ [m]"
    if ny==1:
       ylabel = r"$y$ \ [m]"
    if ny==2:
       ylabel = r"$z$ \ [m]"

    
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    
    clipped_field_np = np.clip(field_np,minF,maxF)
    
    nColors = 32;
    nTicks = 10;
    colorLevels = np.linspace(minF-0.0001,maxF+0.0001,nColors,endpoint=True)
    ticks = np.linspace(minF-0.0001,maxF+0.0001,nTicks,endpoint=True)
    
    ax.tricontour(x,y,trianglesn,clipped_field_np,colorLevels,linewidths=0.5,colors="k")
    cntr = ax.tricontourf(x,y,trianglesn,clipped_field_np,colorLevels)

    divider = make_axes_locatable(ax)
    color_axis = divider.append_axes("right", size="5%", pad=0.1)
    cbar = fig.colorbar(cntr,cax=color_axis,ticks=ticks,format=ticker.FormatStrFormatter('%2.2f'))

    #print titleTex
    #print fieldNameTex
    
    cbar.set_label(fieldNameTex,labelpad=20,rotation=270)
    
    #ticker.set_major_formatter(major_formatter)
    
    plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
    
    figPath = join(casePath,"analysis",fieldName+"_"+title+".png")
    print figPath
    plt.savefig(figPath,bbox_inches='tight')
    
    plt.clf()
    plt.close()

if __name__ == "__main__":

    #~ parser = ArgumentParser()

    #~ parser.add_argument("-f","--file",dest="filePath",help="File path",metavar="FILE")
    #~ parser.add_argument("-x","--rangex",dest="rangex",nargs="+",help="Range x",metavar="RX",type=float)
    #~ parser.add_argument("-y","--rangey",dest="rangey",nargs="+",help="Range y",metavar="RY",type=float)   
    #~ parser.add_argument("-s","--ranges",dest="ranges",nargs="+",help="Range scalar",metavar="RS",type=float)

    #~ args  = parser.parse_args()

    #~ filePath  = args.filePath
    #~ rangeX = args.rangex
    #~ rangeY = args.rangey
    #~ rangeScalar = args.ranges

    #~ sample application

    fieldName = "magUMean"
    fieldNameTex = r"$|\overline{\mathbf{u}}|$"
    #timeName="50000" # foamListTime -latestTime
    rangeField=[0,0.6]
     
    timeName = subprocess.check_output("foamListTimes -latestTime",shell=True).replace("\n","")
     
    titles = ["x=1p5","x=1p0","x=0p5","ytoB=0","ytoB=1to3","ytoB=2to3"]
    titlesTex=[ r"$x=1.0\,m$",r"$x=1.0\,m$",r"$x=0.5\,m$",r"$y/B=0$",r"$y/B=1/3$",r"$y/B=2/3$"]

    rangesx = [[0,0.7],[0,0.7],[0,0.7],[-0.5,10],[-0.5,10],[-0.5,10]]
    nxs = [1,1,1,0,0,0]
    rangesy = [[-0.46,0],[-0.46,0],[-0.46,0],[-0.46,-0.3],[-0.46,-0.3],[-0.46,-0.3]]
    nys = [2,2,2,2,2,2]
    sizes=[(6,5),(6,5),(6,5),(15,5),(15,5),(15,5)]
    
    
    for i in range(len(titles)):
        title = titles[i]
        titleTex = titlesTex[i]
        rangex = rangesx[i]
        rangey = rangesy[i]
        nx = nxs[i]
        ny = nys[i]
        size = sizes[i]
        main(fieldName,fieldNameTex,title,titleTex,timeName,rangex,nx,rangey,ny,rangeField,size)
    

