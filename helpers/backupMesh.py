#! /bin/python
# backupMesh.py
# does the backup of the mesh

import subprocess as sp
from os.path import join
import os

def main(case):

    mesh1 = join(case,"constant/polyMesh")
    mesh2 = join(case,"constant/polyMesh_bak")
    sp.call(["cp","-r",mesh1,mesh2])

if __name__ == "__main__":
    main(os.getcwd())
