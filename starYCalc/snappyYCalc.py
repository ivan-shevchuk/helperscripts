#! /bin/python

import os,sys

# running the script
# python snappyYCalc.py 0.2 14 1.25

# python snappyYCalc.py 0.078 0.7 14 1.25 
#                       deltaS finalLayerThickness nLayers expansionRatio

# output: delta1, total blThickness


deltaS = float(sys.argv[1])
finalLayerThickness = float(sys.argv[2]) # Total thickness of BL mesh
nLayers = int(sys.argv[3]) # number of layers in it
expRatio = float(sys.argv[4]) # growth ratio

delta1 = deltaS*finalLayerThickness*pow(1.0/expRatio,nLayers)/2;

S = 2*delta1*(pow(expRatio,nLayers)-1)/(expRatio-1)

print "Delta1 =",delta1
print "BlThickness=",S
