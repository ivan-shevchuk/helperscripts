#!/usr/bin/python

import os,re,sys
from os import path

def isfloat(x):
 try:
  float(x)
  return True;
 except:
  return False;

def float_cmp_toint(x):
  if x>0:
    return 1
  elif x<0:
    return -1
  elif x==0:
    return 0

def cmp_foam_times(t1,t2):
    return float_cmp_toint(float(t1)-float(t2))

inputFolder = sys.argv[1] #probe folder
outputFolder = sys.argv[2]

times=filter(isfloat, os.listdir(inputFolder))
times.sort(cmp_foam_times)
print times

filenames=[]
for time in times:
    filenames.append(path.join(inputFolder,str(time)+"/U"))

with open(outputFolder+'/tmp', 'w') as outfile:
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                outfile.write(line)

## Consistency filter - filters out time jumps in concatenated files

inputFile=outputFolder+'/tmp'
outputFile=outputFolder+'/U'

input=open(inputFile,'r')
output=open(outputFile,'w')

iter=0
lastConsistentTime=0.00000

for lines in input:
  ## keep the head of the file
  if(iter>=4):         
    newLines = lines.replace("("," ")
    newLines = newLines.replace(")"," ")
    newLinesSplit = newLines.split();
    #print newLinesSplit
    if(newLinesSplit[0]!="#"):
      ## ensure that the plots will be time-consistent (no jumps in time)
      if ( float(newLinesSplit[0]) > lastConsistentTime ):
         output.write(newLines)
         lastConsistentTime=float(newLinesSplit[0])
  iter=iter+1

input.close()	
output.close()
 
os.system("rm -r "+inputFile)


