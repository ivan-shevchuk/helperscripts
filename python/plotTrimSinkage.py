
import sys,os,shutil,re
from math import *
from argparse import ArgumentParser
from os.path import join
import copy

from math import *
from vtk import *
import numpy as np

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages

import commonVariables as comv
from commonVariables import tex,isfloat,float_cmp_toint,cmp_foam_times

def readMotionDat(casePath,inputFolder):

    outputFolder = join(casePath,comv.plottingPath,comv.shipMotionOutputFile+"_"+comv.shipPatch)
    
    print outputFolder
    
    if os.path.exists(outputFolder):
        shutil.rmtree(outputFolder)

    os.makedirs(outputFolder)
    times=filter(isfloat, os.listdir(inputFolder))
    times.sort(cmp_foam_times)
    print times
    
    filenames=[]
    for time in times:
        filenames.append(join(inputFolder,str(time),comv.shipMotionOutputFile+".dat"))
        
    alllines = [];
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                alllines.append(line)
    
    iter=0
    lastConsistentTime=0.00000
    conti_lines = []
    output=open(join(outputFolder,comv.shipMotionOutputFile+"_"+comv.shipPatch+".dat"),'w')
    #Time   ForceResidual   MomentResidual  Heave, m   TrimAngle, deg
    
    #~ print alllines

    for line in alllines:
       ## keep the head of the file
       if(iter>=1):
           newLinesSplit = line.split();
           ## ensure that the plots will be continuous (no jumps in time)
           if ( float(newLinesSplit[0]) > lastConsistentTime ):          
               conti_lines.append(map(float,newLinesSplit))
               output.write(line)
               lastConsistentTime=float(newLinesSplit[0])
       iter+=1
      
    output.close()
    return conti_lines

#Time   ForceHDyn  ForceHStatic  ForceGravity  ForceRes  MomentHDyn  MomentHStat  MomentRes

def main(casePath):
    
    motionPath = comv.shipMotionOutputFile+"_"+comv.shipPatch
    inputFolder = join(casePath,motionPath)
    caseParameters = ParsedParameterFile(join(casePath,comv.caseParametersFile),noHeader=False)
    l = 40
    FnH = caseParameters["FnH"]
    L   = caseParameters["lRef"]
    HtoT= caseParameters["HtoT"]
    
    lines = np.array(readMotionDat(casePath,inputFolder))
    
    comv.setTexStyle()  
    entities=["Trim","Sinkage"]
    
    for entity in entities:
    
        if entity=="Trim":
        
            t  = (lines.T)[0] # time
            psi = (lines.T)[4] 
            xr = [min(t), max(t)]
                
            plt.title(entity +","+ tex("\ Fn_H="+str(FnH)) + tex(", \ H/T="+str(HtoT)))
            
            psiplot, = plt.plot(t,psi)
    
            plt.ylabel(tex('\psi \ [^{\circ}]'))
            plt.xlabel(tex('t \ [iter]'))
            
            plt.xlim(xr)
            
        if entity=="Sinkage":
        
            t  = (lines.T)[0] # time
            dT = (lines.T)[3] 

            xr = [min(t), max(t)]
                
            plt.title(entity +","+ tex("\ Fn_H="+str(FnH)) + tex(", \ H/T="+str(HtoT)))
            
            plt.plot(t,dT)
    
            plt.ylabel(tex('\Delta T \ [m]'))
            plt.xlabel(tex('t \ [iter]'))
            
            plt.xlim(xr)

            
        plt.grid()
        outPath = join(casePath,comv.plottingPath,entity)+".pdf"
        out = PdfPages(outPath)
        plt.savefig(out,format='pdf')
        out.close()
        plt.close()
        os.system("okular "+ outPath + " &")
    
if __name__ == "__main__":

    parser = ArgumentParser()
        
    parser.add_argument("-c","--case",dest="casePath",help="Case path",metavar="FILE",default="./")

    args  = parser.parse_args()
    
    casePath  = args.casePath
    #inputFile = join(casePath)
    
    main(casePath)
