#! /bin/sh


import sys,os,shutil,re
from math import *
from argparse import ArgumentParser
from os.path import join
import copy

from math import *
from vtk import *
import numpy as np

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages

import commonVariables as comv
from commonVariables import tex,isfloat,float_cmp_toint,cmp_foam_times

def readForcesDat(casePath,entityName):

    inputFolder = join(casePath,comv.postProcessingPath,entityName)
    outputFolder = join(casePath,comv.plottingPath,entityName)
    
    print outputFolder
    
    if os.path.exists(outputFolder):
        shutil.rmtree(outputFolder)

    os.makedirs(outputFolder)
    # a folder, where the filtered concatenated forces are

    times=filter(isfloat, os.listdir(inputFolder))
    times.sort(cmp_foam_times)
    print times
    
    filenames=[]
    for time in times:
        filenames.append(join(inputFolder,str(time),entityName+".dat"))
        
    alllines = [];
    for fname in filenames:
        with open(fname) as infile:
            for line in infile:
                alllines.append(line)
    
    iter=0
    lastConsistentTime=0.00000
    
    conti_lines = []
    
    output=open(join(outputFolder,entityName+".dat"),'w')

    for line in alllines:
      ## keep the head of the file
      if(iter>=4):
        newLines = line.replace("("," ")
        newLines = newLines.replace(")"," ")
        newLinesSplit = newLines.split();
        #print newLinesSplit
        if(newLinesSplit[0]!="#"):
          ## ensure that the plots will be continuous (no jumps in time)
          if ( float(newLinesSplit[0]) > lastConsistentTime ):
             if entityName=="forces":             
                 conti_lines.append(map(float,[newLinesSplit[0],newLinesSplit[1],newLinesSplit[4]]))
             elif entityName=="forceCoeffs": 
                 conti_lines.append(map(float,[newLinesSplit[0],newLinesSplit[2]]))
                
             output.write(newLines)
             lastConsistentTime=float(newLinesSplit[0])
      iter+=1
      
    output.close()
    return conti_lines

def main(casePath="./"):

    baseDir=join(casePath,comv.forcesPath)
    postProcessingDir=join(casePath,comv.postProcessingPath)
    caseParameters = ParsedParameterFile(join(casePath,comv.caseParametersFile),noHeader=False)
       
    entities=["forces","forceCoeffs"]
    settings={"forces":{"sign":-1,"range":[0.0,50.0]},"forceCoeffs":{"sign":1,"range":[0.0,0.02]}}
    
    for entity in entities:
    
        lines = np.array(readForcesDat(casePath,entity))
        
        comv.setTexStyle()   
        
        l=40
        FrH = caseParameters["FrH"]
        L   = caseParameters["lRef"]
        HtoT= caseParameters["HtoT"]
        
        if entity=="forces":
        
            t  = (lines.T)[0] # time
            fp = (lines.T)[1] * settings[entity]["sign"] # fxp
            fv = (lines.T)[2] * settings[entity]["sign"] # fxv
            ftot = np.add(fp,fv)
            
            xr = [min(t), max(t)]
            yr = settings[entity]["range"]
                
            plt.title(entity +","+ tex("\ Fn_H="+str(FrH)) + tex(", \ H/T="+str(HtoT)))
            
            fp, = plt.plot(t,fp,label=tex('F_P'))
            fv, = plt.plot(t,fv,label=tex('F_V'))
            ftot, = plt.plot(t,ftot,label=tex('F_T'))
            
            plt.ylabel(tex('F \ [N]'))
            plt.xlabel(tex('t \ [iter]'))
            
            plt.xlim(xr)
            plt.ylim(yr)
            plt.legend()
            
        if entity=="forceCoeffs":
        
            t  = (lines.T)[0] # time
            ct = (lines.T)[1] * settings[entity]["sign"] # fxp
            
            xr = [min(t), max(t)]
            yr = settings[entity]["range"]
                
            plt.title(entity +","+ tex("\ Fn_H="+str(FrH)) + tex(", \ H/T="+str(HtoT)))
            
            plt.plot(t,ct)

            plt.ylabel(tex('C_T \ [-]'))
            plt.xlabel(tex('t \ [iter]'))
            
            plt.xlim(xr)
            plt.ylim(yr)
            

        plt.grid()
        outPath = join(casePath,comv.plottingPath,entity)+".pdf"
        out = PdfPages(outPath)
        plt.savefig(out,format='pdf')
        out.close()
        plt.close()
        os.system("okular "+ outPath + " &")


if __name__ == "__main__":

    parser = ArgumentParser()
        
    parser.add_argument("-c","--case",dest="casePath",help="Case path",metavar="FILE",default="./")

    args  = parser.parse_args()
    
    casePath  = args.casePath
    
    main(casePath)
