
import sys,os,shutil,re
from math import *
from argparse import ArgumentParser
from os.path import join
import copy

from math import *
from vtk import *
import numpy as np

from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.backends.backend_pdf import PdfPages

import commonVariables as comv
from commonVariables import tex,isfloat,float_cmp_toint,cmp_foam_times

def analyzeVtk(casePath,points):

# Import timedirectories
# read the vtk directory and get all the time steps and return list 

    basedir = join(join(casePath,comv.postProcessingPath),comv.waveElevationPath)
    
    timesteps=[]
    for root,dir,file in os.walk(basedir,True):
     p,time = os.path.split(root)
     if (bool(re.search("[0-50]",time))):
      timesteps.append(time)
    
    #print timesteps
    
    timesteps.sort(cmp_foam_times) # This sorts by float numbers
    ts = timesteps[-1]
    
    #print ts
    filename = file[0]
    
    readfile = join(basedir,ts,filename)
    
    reader = vtkPolyDataReader()
    reader.SetFileName(readfile)
    reader.Update()
    output = reader.GetOutput()
    
    outputPoints = copy.deepcopy(points)
    
    for i in range(len(points)):
    
        #- Coordinate to find closest point
        zfind = [points[i][0], points[i][1], points[i][2]]
        #- Find point
        p = output.FindPoint(zfind)
        #- Coordinate of point
        zfound = output.GetPoint(p)
    
        outputPoints[i] = [points[i][0], points[i][1] ,  zfound[2]]  
    
    return outputPoints
    
def main(casePath,inputFile):

    ssPath=comv.plottingPath
    outputFolder=join(casePath,ssPath,comv.waveElevationPath)
    
    if os.path.exists(outputFolder):
        shutil.rmtree(outputFolder)
        os.mkdir(outputFolder)
             
    caseParameters = ParsedParameterFile(join(casePath,comv.caseParametersFile),noHeader=False)
    weParams = caseParameters["analysis"][comv.waveElevationPath]

    y  = weParams["y"]
    x1 = weParams["x1"]
    x2 = weParams["x2"]
    npoints = 500 #weParams["npoints"]    
    
    px = (np.linspace(x1,x2, num=npoints, endpoint=True))[np.newaxis]
    py = np.array([y]*npoints)[np.newaxis]
    
    analysisPoints = np.hstack((px.T,py.T,py.T))
    
    outputPoints = analyzeVtk(casePath,analysisPoints) # find z point of the free surface
    
    # plotting
    
    comv.setTexStyle()  
    
    l=40
    
    FnH = caseParameters["FnH"]
    L   = caseParameters["lRef"]
    
    x = l*outputPoints.T[0]
    y = l*outputPoints.T[2]
    
    xr = [x1*l,x2*l]
    yr = [-1.0,0.5]
    
    plt.title("Wave elevation, y = 39m ")
    
    waveLabel=tex('Fn_{H } = '+ str(FnH))
    wave, = plt.plot(x,y,label=waveLabel)
    
    bowline, = plt.plot([L*l,L*l],yr,label="Bow", color='k',lw=1.5)
    sternline, = plt.plot([0.0,0.0],yr,label="Stern", color='k',linestyle='--',lw=1.5)
    
    plt.ylabel(tex('\zeta \ [m]'))
    plt.xlabel(tex('x \ [m]'))
    
    plt.xlim(xr[::-1])
    plt.ylim(yr)
    
    plt.yticks(np.arange(yr[0],yr[1], 0.1))
    plt.xticks()
    
    wave_legend = plt.legend([wave],[waveLabel],loc=1)
    stern_legend = plt.legend([sternline],["Stern"],loc=4)
    bow_legend = plt.legend([bowline],["Bow"],loc=3)
    
    plt.gca().add_artist(wave_legend)
    plt.gca().add_artist(stern_legend)
    
    plt.grid()
    
    outPath = join(casePath,comv.plottingPath,comv.waveElevationPath)+".pdf"
    
    out = PdfPages(outPath)
    plt.savefig(out,format='pdf')
    out.close()
    os.system("okular "+ outPath + " &")

if __name__ == "__main__":

    parser = ArgumentParser()
        
    parser.add_argument("-c","--case",dest="casePath",help="Case path",metavar="FILE",default="./")

    args  = parser.parse_args()
    
    casePath  = args.casePath
    inputFile = join(casePath,comv.caseParametersFile)
    
    main(casePath,inputFile)
