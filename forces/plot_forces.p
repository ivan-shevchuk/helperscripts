reset

set terminal postscript eps color enhanced "Helvetica,20"  size 5,4
set autoscale

set key width +2
lW = 2

set title plotTitle
set key top right
set key reverse
set key width +2

set grid
set xlabel "t"
set ylabel "N"

set yrange [*:*]
set xrange [xmin:xmax]

set xtics 1.0

set output outputFile

set title 'Forces'

set style line 1 linewidth 0.5 linecolor 1
set style line 2 linewidth 0.5 linecolor 2

plot inputFile u 1:(-1*$2) t 'F_p'  w l lt 1  lw lW lc rgb "blue" , \
     inputFile u 1:(-1*$5) t 'F_v'  w l lt 1  lw lW lc rgb "red"  

