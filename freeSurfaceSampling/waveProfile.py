#!/usr/bin/python

# waveProfile
# Read VTK files with isosurface 
# Track the wave elevation in (x,y) coordinate

import os
import re
from vtk import *
from optparse import OptionParser
from numpy import *

def float_cmp_toint(x):
  if x>0:
    return 1
  elif x<0:
    return -1
  elif x==0:
    return 0

def cmp_foam_times(t1,t2):
    return float_cmp_toint(float(t1)-float(t2))

print "waveProfile v1.0 - changed version of elevationVsTime v0.2"

# Read command line arguments
parser = OptionParser()

parser.add_option("--if","--input-file",dest="points",type="string",help="Filename containing coordinates",metavar="FILE",default="points")
parser.add_option("-n","--n-points",dest="n",type="int",help="Number of the discretization points",default=100)
parser.add_option("--of","--output-file",dest="output",type="string",help="Output file for further analysing with gnuplot",metavar="OFILE",default="waveElevation/elevationData")

(options, args) = parser.parse_args()

## Generating sampling points
print "Reading points from file \"",options.points,"\""
f = loadtxt(options.points)

#- Search starting point
x = f[:,0]
y = f[:,1]
#n = f[:,2]

npoints = options.n

print "Point1 - ",x[0]," ",y[0]
print "Point2 - ",x[1]," ",y[1]
print npoints," discretization points"

coordsx = [None]*(npoints+1)
coordsy = [None]*(npoints+1)
coordsz = [None]*(npoints+1)

dx = x[1]-x[0]
dy = y[1]-y[0]

#- List input points
for i in range(0, npoints+1):
   coordsx[i] = x[0]+(i*dx)/npoints
   coordsy[i] = y[0]+(i*dy)/npoints
   coordsz[i] = 0.0
   "Point ",i," (",coordsx[i],",",coordsy[i],")"

#- Search starting point
npoints = len(coordsx)

# Import timedirectories
# read the vtk directory and get all the time steps and return list 
basedir = "../postProcessing/freeSurface/"

timesteps=[]
for root,dir,file in os.walk(basedir,True):
 p,time = os.path.split(root)
 if (bool(re.search("[0-50]",time))):
  timesteps.append(time)

# timesteps = sorted(timesteps)
print timesteps

filename = file[0]
basename = options.output

timesteps.sort(cmp_foam_times) # This sorts by float numbers
print timesteps

ts = timesteps[-1]

print ts

##for ts in timesteps:
  #- Counter info -- this info is approximate
  # counter = counter+1
  # if ( counter%frac == 0 & frac > 0 ):
  # print round(counter/frac)*10,"%"

  #- Read VTK file 
readfile = basedir + ts + "/" + filename
reader = vtkPolyDataReader()
reader.SetFileName(readfile)
reader.Update()
output = reader.GetOutput()

writefile = basename
#os.system("rm -r elevationData")

file = open(writefile,'w')

for i in range(len(coordsx)):

    #- Coordinate to find closest point
    zfind = [coordsx[i], coordsy[i], coordsz[i]]

    #- Find point
    p = output.FindPoint(zfind)

    #- Coordinate of point
    zfound = output.GetPoint(p)
    coordsz[i] = zfound[2]

    #- Write to file
    print >> file, coordsx[i],coordsy[i], coordsz[i]

file.close()


#os.system("gnuplot plot_elevation.p")
#os.system("okular elevation.ps &" )

