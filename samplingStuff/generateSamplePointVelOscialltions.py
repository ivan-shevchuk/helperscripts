
# Generates sampling points for velocity oscillations

import sys
import os

from math import *
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

x  =  3.065
y1 =  1.650
y2 =  3.950
z  = -1.375

R  = 0.525

rRList= [0.4,0.6,0.8,1.0]

case  = sys.argv[1]

samplingStuffPath = os.path.join(case,"samplingStuff")

writefile1 = os.path.join(samplingStuffPath,"velocityOscProbePointsProp1")
writefile2 = os.path.join(samplingStuffPath,"velocityOscProbePointsProp2")

file1= open(writefile1, 'w')
file2= open(writefile2, 'w')

i = 0
for phi in [0,90,180,270]:
    for rR in rRList:
        r = rR * R

        pX  = x + 0.0

        pY1 = y1 + r*math.sin(math.radians(phi))
        pY2 = y2 + r*math.sin(math.radians(phi))

        pZ  = z + r*math.cos(math.radians(phi))

        print >>file1,"( ",pX,pY1,pZ," )"
        print >>file2,"( ",pX,pY2,pZ," )"


