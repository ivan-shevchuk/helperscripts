
# Generates sampling points for velocity oscillations

import sys
import os
import copy

import numpy as np

from math import *
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

from quaternion import *

x  =  3.065
y1 =  1.650
y2 =  3.950
z  = -1.375

CoG = np.array([67.5, 0.0, -1])
R  = 0.525
rRList= [0.4,0.6,0.8,1.0]
directions=["right","left","up","down"]

case  = sys.argv[1]

trim    = float(sys.argv[2]) # [deg] should be set in degrees, will be later converted to rads
sinkage = float(sys.argv[3]) # [m]

samplingStuffPath = os.path.join(case,"samplingStuff")

probesProp1 = os.path.join(samplingStuffPath,"probesProp1")
probesProp2 = os.path.join(samplingStuffPath,"probesProp2")

probesTemplatePath=os.path.join(case,"samplingStuff/probes.template")

os.system("rm  " + probesProp1 )
os.system("cp  " + probesTemplatePath + " " + probesProp1 )
os.system("rm  " + probesProp2 )
os.system("cp  " + probesTemplatePath + " " + probesProp2 )

probesDict1 = ParsedParameterFile(probesProp1,noHeader=True)
probesDict2 = ParsedParameterFile(probesProp2,noHeader=True)

dictTemplate={"type":"probes","functionObjectLibs":"(\"libsampling.so\")"
    ,"fields":["U"],"outputControl":"timeStep"
    ,"outputInterval":"$probeInterval","probeLocations":[]}


for i,phi in enumerate([0.0,90.,180.,270.]):

    currentEntryName1 = "probes_"+"prop1_"+directions[i]
    currentEntryName2 = "probes_"+"prop2_"+directions[i]
    
    probesDict1[currentEntryName1]=copy.deepcopy(dictTemplate)
    probesDict2[currentEntryName2]=copy.deepcopy(dictTemplate)
    
    probeLocations1=[]
    probeLocations2=[]

    for rR in rRList:

        r = rR * R
        pX  = x + 0.0
        pY1 = y1 + r*math.sin(math.radians(phi))
        pY2 = y2 + r*math.sin(math.radians(phi))
        pZ  = z + r*math.cos(math.radians(phi))
        
        p1 = np.array( [pX, pY1, pZ] )
        p2 = np.array( [pX, pY2, pZ] )
        
        p1_os = CoG - p1 # coordinates of a vector in a ship-fixed system,..
        p2_os = CoG - p2 # ..this vector should be rotated
        
        p1_os_rotated = rotateVector(p1_os,[0.0,1.0,0.0],radians(trim))
        p2_os_rotated = rotateVector(p2_os,[0.0,1.0,0.0],radians(trim))
        
        p1_transformed = CoG - p1_os_rotated + np.array([0.0,0.0,sinkage]) 
        p2_transformed = CoG - p2_os_rotated + np.array([0.0,0.0,sinkage]) 
        
        probeLocations1.append([p1_transformed[0],p1_transformed[1],p1_transformed[2]])
        probeLocations2.append([p2_transformed[0],p2_transformed[1],p2_transformed[2]])
        
    probesDict1[currentEntryName1]["probeLocations"] = probeLocations1
    probesDict2[currentEntryName2]["probeLocations"] = probeLocations2
        
probesDict1.writeFile()
probesDict2.writeFile()

