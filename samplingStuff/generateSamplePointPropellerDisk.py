
# Generates sample points on circles with radii r= 0.4 - 1.0R
# see Gothenburg proceedings vol II, p 50 

import sys
import os

from math import *
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

x  =  3.065
y1 =  1.650
y2 =  3.950
z  = -1.375

R = 0.525

rRList= [0.4,0.6,0.8,1.0]

phiStepDeg = 5.0
phiStepRad = math.radians(phiStepDeg)

case = sys.argv[1]

sampleDictPath1=os.path.join(case,"system/sampleDictPropDisk1")
sampleDictPath2=os.path.join(case,"system/sampleDictPropDisk2")

sampleDictTemplatePath=os.path.join(case,"samplingStuff/sampleDict.template")


os.system("rm  " + sampleDictPath1 )
os.system("cp  " + sampleDictTemplatePath + " " + sampleDictPath1 )
os.system("rm  " + sampleDictPath2 )
os.system("cp  " + sampleDictTemplatePath + " " + sampleDictPath2 )


sampleDict1 = ParsedParameterFile(sampleDictPath1)
sets1       = sampleDict1["sets"]
sampleDict2 = ParsedParameterFile(sampleDictPath2)
sets2       = sampleDict2["sets"]


#print sets
pointSet1 = []
pointSet2 = []

samplingStuffPath = os.path.join(case,"samplingStuff")
writefile = os.path.join(samplingStuffPath,"phiValues")
file = open(writefile, 'w')

phi = 0
#- List input points
while  phi <= 2*3.1415926 + phiStepRad:      
    print >> file, degrees(phi)
    phi = phi + phiStepRad

i = 0
for rR in rRList:
 
    name1 = "prop1_"+"rR"+str(rR)
    name2 = "prop2_"+"rR"+str(rR)

    pointSet1 = []
    pointSet2 = []

    phi = 0
    r = rR * R

    while  phi <= 2*3.1415926 + phiStepRad:
        pointSet1.append([ x+0.0, y1+r*math.sin(phi), z+r*math.cos(phi)] )
        pointSet2.append([ x+0.0, y2+r*math.sin(phi), z+r*math.cos(phi)] )
        phi = phi + phiStepRad

    #print name1, name2

    sets1.append(name1)
    sets1.append({'type':'cloud','name':'pl', 'axis':'xyz', 'points':pointSet1})
    sets2.append(name2)
    sets2.append({'type':'cloud','name':'pl', 'axis':'xyz', 'points':pointSet2})

    #print pointSet1    
    #print "------------------"
    #print pointSet2  
    #print "------------------"

    i = i+1

sampleDict1.writeFile();
sampleDict2.writeFile();
