
# Generates sample points on circles with radii r= 0.4 - 1.0R
# see Gothenburg proceedings vol II, p 50 

import sys
import os
import numpy as np

from math import *
from os import path
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

from quaternion import *

x  =  3.065
y1 =  1.650
y2 =  3.950
z  = -1.375

R = 0.525

rRList= [0.4,0.6,0.8,1.0]

CoG = np.array([67.5, 0.0, -1])

phiStepDeg = 5.0
#phiStepDeg = 90
phiStepRad = math.radians(phiStepDeg)

case    = sys.argv[1]
trim    = float(sys.argv[2]) # [deg] should be set in degrees, will be later converted to rads
sinkage = float(sys.argv[3]) # [m])

#sampleDictPath1=os.path.join(case,"system/sampleDictPropDisk1")
#sampleDictPath2=os.path.join(case,"system/sampleDictPropDisk2")

sampleDictPath1=os.path.join(case,"samplingStuff/sampleDictPropDisk1")
sampleDictPath2=os.path.join(case,"samplingStuff/sampleDictPropDisk2")

sampleDictTemplatePath=os.path.join(case,"samplingStuff/sampleDict.template")

os.system("rm  " + sampleDictPath1 )
os.system("cp  " + sampleDictTemplatePath + " " + sampleDictPath1 )
os.system("rm  " + sampleDictPath2 )
os.system("cp  " + sampleDictTemplatePath + " " + sampleDictPath2 )

sampleDict1 = ParsedParameterFile(sampleDictPath1)
sets1       = sampleDict1["sets"]
sampleDict2 = ParsedParameterFile(sampleDictPath2)
sets2       = sampleDict2["sets"]

#print sets
pointSet1 = []
pointSet2 = []

samplingStuffPath = os.path.join(case,"samplingStuff")
writefile = os.path.join(samplingStuffPath,"phiValues")
file = open(writefile, 'w')

phi = 0
#- List input points
while  phi <= 2*3.1415926 + phiStepRad:      
    print >> file, degrees(phi)
    phi = phi + phiStepRad

i = 0
for rR in rRList:
 
    name1 = "prop1_"+"rR"+str(rR)
    name2 = "prop2_"+"rR"+str(rR)

    pointSet1 = []
    pointSet2 = []

    phi = 0
    r = rR * R

    while  phi <= 2*3.1415926 + phiStepRad:
    
        p1 = np.array( [x+0.0, y1+r*math.sin(phi), z+r*math.cos(phi)] )
        p2 = np.array( [x+0.0, y2+r*math.sin(phi), z+r*math.cos(phi)] )
        
        p1_os = CoG - p1 # coordinates of a vector in a ship-fixed system,..
        p2_os = CoG - p2 # ..this vector should be rotated
        
        p1_os_rotated = rotateVector(p1_os,[0.0,1.0,0.0],radians(trim))
        p2_os_rotated = rotateVector(p2_os,[0.0,1.0,0.0],radians(trim))
        
        p1_transformed = CoG - p1_os_rotated + np.array([0.0,0.0,sinkage]) 
        p2_transformed = CoG - p2_os_rotated + np.array([0.0,0.0,sinkage]) 
        
        pointSet1.append( [p1_transformed[0],p1_transformed[1],p1_transformed[2]] )
        pointSet2.append( [p2_transformed[0],p2_transformed[1],p2_transformed[2]] )
        
        phi = phi + phiStepRad

    #print name1, name2
    
    # Block, translating and rotating the points about the ship CoG

    sets1.append(name1)
    sets1.append({'type':'cloud','name':'pl', 'axis':'xyz', 'points':pointSet1})
    sets2.append(name2)
    sets2.append({'type':'cloud','name':'pl', 'axis':'xyz', 'points':pointSet2})

    #print pointSet1    
    #print "------------------"
    #print pointSet2  
    #print "------------------"

    i = i+1

sampleDict1.writeFile();
sampleDict2.writeFile();
