reset

set terminal postscript eps color enhanced "Helvetica,16"

set output "omega.ps"

set logscale
set xr [*:*]
set yr [*:0.024]

set title "NASA Benchmark, {/Symbol w} comparison, x=1m"
set xlabel "{/Symbol w }"
set ylabel "y"

set style line 1 linewidth 1.5 linecolor 1
set style line 2 linewidth 1.5 linecolor 2
set style line 3 linewidth 1.5 linecolor 3
set style line 4 linewidth 1.5 linecolor 4
set style line 5 linewidth 1.5 linecolor 5
set style line 6 linewidth 1.5 linecolor 7


plot "sets/y_k_omega_coarse_nondim.xy" u 3:1 with lines ls 1 title "{OpenFOAM,coarse", \
     "sets/y_k_omega_medium_linUp_nondim.xy" u 3:1 with lines ls 2 title "{OpenFOAM,medium,linearUpwind", \
     "sets/y_k_omega_medium_limLin_nondim.xy" u 3:1 with lines ls 3 title "{OpenFOAM,medium,limitedLinear", \
     "sets/medium_limitedLinear0.5/y_k_omega_nondim.xy" u 3:1 with lines ls 4 title "{OpenFOAM,medium,limitedLinear0.5", \
     "sets/fine_upwind/y1m_k_omega_nondim.xy" u 3:1 with lines ls 5 title "{OpenFOAM,fine,limitedLinear", \
     "sets/nasa_x_y_omega_k_nondim.dat" u 3:2 with lines ls 6 title " NASA(CFL3D)"

## sets/d_k_omega.xy' u ($3*0.0000132/(331^2)):1 with lines ls 2 title 'L, I = 2% -wrong!!
