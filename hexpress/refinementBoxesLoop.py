igg_script_version(2.1)

HXP.delete_all_refinement_boxes()

# SHIP BOXES

x0 =-600
#x1 =-300
#x2 =-100
#x3 = 200
#x4 = 350
xN = 600

y0 =  0
#y2 =  100
#y3 =  250
yN =  400

# x1 y1 z1 x2 y2 z2, [target_size_x, target_size_y, target_size_z], refinementLevel, volumic?]
boxList=[
         # Ship Boxes
         [   -5,  y0,   -3, 140, 7.0, 1.75,    [0.2,0.1,0.1], 11, 0],
         [   -5,  y0, -2.5,  20, 7.0,    0, [0.05,0.05,0.05], 12, 0],
         [   -5,  y0,    0, 140, 7.0,    9,    [0.4,0.2,0.2], 10, 0],
         # WP refinement boxes
         # Level 10
         [ -600,  y0, -0.7, 600, 400,  0.7,    [10,  10,0.2], 9, 1],
         [ -400,  y0, -1.0, 450, 200,  1.0,    [ 5,   5,0.2], 10, 1],
         [ -200,  y0, -1.5, 300,  75,  1.5,  [ 2.5 ,2.5,0.2], 11, 1],
         # Level 11
         [ -600,  y0, -0.4, 600, 400,  0.4,    [10 , 10,0.1], 9, 1],
         [ -400,  y0, -0.6, 450, 200,  0.6,     [5 ,  5,0.1], 10, 1],
         [ -200,  y0, -1.0, 300,  75,  1.0,    [2.5,2.5,0.1], 11, 1]
         #[ -600,  y0, -0.4, 600, 400,  0.4,      [5 ,5 ,0.2], 11, 1],
         #[ -100,  y0, -1.5, 300, 250,  1.5,       [ 5,5,0.1], 11, 1],
         #[ -300,  y0, -0.5, 350, 250,  0.5,      [2.5,5,0.1], 11, 1],
         #[ -100,  y0, -1.5, 200, 100,  1.5,      [1  ,1,0.2], 10, 1],
         #[ -100,  y0, -0.5, 200, 100,  0.5,       [1,1,0.1 ], 11, 1]
        ]
        
for nbox in range(0,len(boxList)):
  xb1= boxList[nbox][0]  
  xb2= boxList[nbox][3]
  yb1= boxList[nbox][1]  
  yb2= boxList[nbox][4]
  zb1= boxList[nbox][2]  
  zb2= boxList[nbox][5]
  HXP.create_refinement_cube(xb1,yb1,zb1,xb2,yb2,zb2)
  targetSizeX=boxList[nbox][6][0]
  targetSizeY=boxList[nbox][6][1]
  targetSizeZ=boxList[nbox][6][2]
  HXP.refinement_box(nbox).set_target_size(targetSizeX,targetSizeY,targetSizeZ)
  refLevel=boxList[nbox][7]
  HXP.refinement_box(nbox).set_refinement_level(refLevel)
  volumic=boxList[nbox][8]
  if volumic==1:
      HXP.refinement_box(nbox).set_adaptation_flags(1,1)




