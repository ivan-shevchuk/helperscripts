igg_script_version(2.1)

HXP.delete_all_refinement_boxes()

# SHIP BOXES

x0 =-600
#x1 =-300
#x2 =-100
#x3 = 200
#x4 = 350
xN = 600

y0 =  0
#y2 =  100
#y3 =  250
yN =  400

boxList=[[-5,0,-3,140,7.0,1.75,[0.2,0.1,0.1],11]]

nbox = 0

HXP.create_refinement_cube(-5,0,-3,140,7.0,1.75)
#HXP.create_refinement_cube(boxList[1][1],boxList[1][2],boxList[1][3],boxList[1][4],boxList[1][5],boxList[1][6])
HXP.refinement_box(nbox).set_target_size(0.2,0.1,0.1)
HXP.refinement_box(nbox).set_refinement_level(11)

nbox = nbox + 1

HXP.create_refinement_cube(-5,0,-2.5,10,7.0,-0.5)
HXP.refinement_box(nbox).set_target_size(0.05,0.05,0.05)
HXP.refinement_box(nbox).set_refinement_level(12)

nbox = nbox + 1

HXP.create_refinement_cube(-5,0,0,140,7.0,9)
HXP.refinement_box(2).set_target_size(0.4,0.2,0.2)
HXP.refinement_box(2).set_refinement_level(10)

nbox = nbox + 1

HXP.create_refinement_cube(-100,y0,-1.5,300,yN,1.5)
HXP.refinement_box(3).set_target_size(5,10,0.2)
HXP.refinement_box(3).set_refinement_level(10)
HXP.refinement_box(3).set_adaptation_flags(1,1)

nbox = nbox + 1

HXP.create_refinement_cube(x0,y0,-1.0,xN,yN,1.0)
HXP.refinement_box(4).set_target_size(5,10,0.2)
HXP.refinement_box(4).set_refinement_level(10)
HXP.refinement_box(4).set_adaptation_flags(1,1)

nbox = nbox + 1

HXP.create_refinement_cube(x0,y0,-0.3,xN,yN,0.3)
HXP.refinement_box(5).set_target_size(5,10,0.1)
HXP.refinement_box(5).set_refinement_level(11)
HXP.refinement_box(5).set_adaptation_flags(1,1)

nbox = nbox + 1

HXP.create_refinement_cube(-100,0,-1.5,300,2,1.5)
HXP.refinement_box(6).set_target_size(2.5,5,0.2)
HXP.refinement_box(6).set_refinement_level(10)
HXP.refinement_box(6).set_adaptation_flags(1,1)

nbox = nbox + 1

HXP.create_refinement_cube(-300,0,-0.5,350,250,0.5)
HXP.refinement_box(7).set_target_size(2.5,5,0.1)
HXP.refinement_box(7).set_refinement_level(11)
HXP.refinement_box(7).set_adaptation_flags(1,1)

nbox = nbox + 1

HXP.create_refinement_cube(-100,0,-1.5,200,100,1.5)
HXP.refinement_box(8).set_target_size(1,1,0.2)
HXP.refinement_box(8).set_refinement_level(10)
HXP.refinement_box(8).set_adaptation_flags(1,1)

nbox = nbox + 1

HXP.create_refinement_cube(-100,0,-0.5,200,100,0.5)
HXP.refinement_box(9).set_target_size(1,1,0.1)
HXP.refinement_box(9).set_refinement_level(11)
HXP.refinement_box(9).set_adaptation_flags(1,1)

nbox = nbox + 1



