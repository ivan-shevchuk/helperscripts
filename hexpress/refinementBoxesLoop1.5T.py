igg_script_version(2.1)

def determineRefinementLevel(targetCellSizes,CellSizes):
    XLevel = -1 ;
    YLevel = -1 ;
    ZLevel = -1 ;
    for i in range(0,len(CellSizes)):
      print CellSizes[i][1] , targetCellSizes[0]
      if CellSizes[i][1] <= targetCellSizes[0] and XLevel == -1:
         XLevel = i
      if CellSizes[i][2] <= targetCellSizes[1] and YLevel == -1:
         YLevel = i
      if CellSizes[i][3] <= targetCellSizes[2] and ZLevel == -1:
         ZLevel = i
    print XLevel, YLevel, ZLevel
    print max([XLevel,YLevel,ZLevel])
    return max([XLevel,YLevel,ZLevel])

HXP.delete_mesh()

Lx=1200.0
Ly=350.0
Lz=12.0

NxInit = 44
NyInit = 16
NzInit = 4

MaxRefLevel = 20

DxInit = Lx/NxInit
DyInit = Ly/NyInit
DzInit = Lz/NzInit

# Build cell sizes table

CellSizes=[]

for n in range(0,MaxRefLevel+1):
    ThisLevel = [n,DxInit/pow(2.0,n),DyInit/pow(2.0,n),DzInit/pow(2.0,n)]
    CellSizes.append(ThisLevel)
    print ThisLevel
    
#determineRefinementLevel([1,1,1],CellSizes)
    
HXP.init_cartesian_mesh(NxInit,NyInit,NzInit)

x0 =-600
#x1 =-300
#x2 =-100
#x3 = 200
#x4 = 350
xN = 600

y0 =  0
#y2 =  100
#y3 =  250
yN =  350


# Refinement level for each box will be calculated automatically
'''
boxList=[
         # Ship Boxes
         [   -5,  y0,   -3, 140, 7.0, 1.75,    [0.22,0.1,0.1], 0],
         [   -5,  y0, -2.5,  20, 7.0,    0, [0.06,0.05,0.05], 1],
         [   -5,  y0,    0, 140, 7.0,    9,    [0.4,0.2,0.2], 0],
         [  1.0, 0.8,-0.22,0.54,0.88,  0.2,    [0.027,0.025,0.025], 0],
         # WP refinement boxes
         # Level 10
         [ -600,  y0,  -1.3, 600, 400,  1.3,    [10,  10,0.2], 1],
         #[ -400,  y0, -1.0, 450, 200,  1.0,    [ 5,   5,0.2], 1],
         #[ -200,  y0, -1.5, 300,  75,  1.5,  [ 2.5 ,2.5,0.2], 1],
         # Level 11
         [ -600,  y0, -0.3, 600, 350,  0.3,     [10 , 10,0.1], 1],
         
         [ -450,  y0, -1.3, 450, 200,  1.3,     [5,   5,0.2], 1],

         #[ -450,  y0, -0.3, 450, 200,  0.3,     [5 ,  5,0.1], 1],
         #[ -400,  y0, -0.3, 450, 200,  0.3,     [5 ,  5,0.1], 1],
         
         [ -150,  y0, -1.3, 250,  100,  1.3,    [5,   5,0.2], 1],
         
         #[ -50,   y0, -1.0, 170,  40,  -1.0,      [1.8 ,1.8,0.1], 1],
         
         #[ -150,  y0, -0.3, 250, 100, -0.3,     [2.5,2.5,0.1], 1],
         #[ -100,  y0, -0.3, 200,  75,  0.3,     [1.8,1.8,0.1], 1]
         #[ -600,  y0, -0.4, 600, 400,  0.4,      [5 ,5 ,0.2], 11, 1],
         #[ -100,  y0, -1.5, 300, 250,  1.5,       [ 5,5,0.1], 11, 1],
         #[ -300,  y0, -0.5, 350, 250,  0.5,      [2.5,5,0.1], 11, 1],
         #[ -100,  y0, -1.5, 200, 100,  1.5,      [1  ,1,0.2], 10, 1],
         #[ -100,  y0, -0.5, 200, 100,  0.5,       [1,1,0.1 ], 11, 1]
        ]
'''        

'''boxList=[
         [   -5,  y0,   -3, 140, 7.0, 1.75,    [0.2,0.1,0.1],0],
         [   -5,  y0, -2.5,  20, 7.0,    0,  [0.057,0.05,0.05],1],
         [   -5,  y0,    0, 140, 7.0,    9,    [0.4,0.2,0.2],1],
         # WP refinement boxes
         # Level 10
         [ -600,  y0, -0.7, 600, 400,  0.7,    [10,  10,0.2],1],
         [ -400,  y0, -1.0, 450, 200,  1.0,    [ 5,   5,0.2],1],
         [ -200,  y0, -1.5, 300,  75,  1.5,  [ 2.5 ,2.5,0.2],1],
         # Level 11
         [ -600,  y0, -0.4, 600, 400,  0.4,    [10 , 10,0.1],1],
         [ -400,  y0, -0.6, 450, 200,  0.6,     [5 ,  5,0.1],1],
         [ -200,  y0, -1.0, 300,  75,  1.0,    [2.5,2.5,0.1],1]
        ]
'''
boxList=[
         [   -5,  y0,   -3, 140, 7.0, 1.75,    [0.22,0.1,0.1], 0],
         [   -5,  y0, -2.5,  20, 7.0,    0, [0.06,0.05,0.05], 1],
         [   -5,  y0,    0, 140, 7.0,    9,    [0.4,0.2,0.2], 0],
         [  1.0, 0.8,-0.22,0.54,0.88,  0.2,    [0.027,0.025,0.025], 0],
         # WP refinement boxes
         # Level 10
         [ -600,  y0,  -1.3, 600, 400,  1.3,    [10,  10,0.2], 1],
         #[ -400,  y0, -1.0, 450, 200,  1.0,    [ 5,   5,0.2], 1],
         #[ -200,  y0, -1.5, 300,  75,  1.5,  [ 2.5 ,2.5,0.2], 1],
         # Level 11
         [ -600,  y0, -0.4, 600, 350,  0.4,     [10 , 10,0.1], 1],
         
         [ -450,  y0, -1.3, 450, 200,  1.3,      [5,   5,0.2], 1],

         [ -450,  y0, -0.4, 450, 200,  0.4,     [5 ,  5,0.1], 1],
         [ -400,  y0, -0.4, 450, 200,  0.4,     [5 ,  5,0.1], 1],
         
         [ -150,  y0, -1.3, 250, 100,  1.3,      [5,   5,0.2], 1],
         
         [ -50,   y0, -1.5, 180,  50,  1.5,      [1.8 ,1.8,0.1], 1],
         [ -25,   y0, -1.5, 160,  25,  1.5,      [0.9 ,0.9,0.1], 1],
         
         [ -150,  y0, -0.4, 250, 100,  0.4,     [2.5,2.5,0.1], 1],
         [ -100,  y0, -0.4, 200,  75,  0.4,     [2.5,2.5,0.1], 1]
        ]
         # WP refinement boxes
maxGlobalNumberOfRefinements = 0;

HXP.open_mesh_step("init")
#HXP.set_prevent_exterior_cells_refinement(1)
HXP.delete_all_adaptation_groups()
HXP.delete_all_refinement_boxes()

for nbox in range(0,len(boxList)):
  xb1= boxList[nbox][0]  
  xb2= boxList[nbox][3]
  yb1= boxList[nbox][1]  
  yb2= boxList[nbox][4]
  zb1= boxList[nbox][2]  
  zb2= boxList[nbox][5]
  HXP.create_refinement_cube(xb1,yb1,zb1,xb2,yb2,zb2)
  volumic=boxList[nbox][7]
  if volumic==1:
      HXP.refinement_box(nbox).set_adaptation_flags(1,1)
  targetSizeX=boxList[nbox][6][0]
  targetSizeY=boxList[nbox][6][1]
  targetSizeZ=boxList[nbox][6][2]
  HXP.refinement_box(nbox).set_target_size(targetSizeX,targetSizeY,targetSizeZ)
  refLevel=determineRefinementLevel(boxList[nbox][6],CellSizes)
  HXP.refinement_box(nbox).set_refinement_level(refLevel)
  if refLevel > maxGlobalNumberOfRefinements:
      maxGlobalNumberOfRefinements = refLevel 

HXP.set_global_number_of_refinements(maxGlobalNumberOfRefinements)

HXP.generate_initial_mesh()
HXP.adapt_mesh()


