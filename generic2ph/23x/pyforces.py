#!/usr/bin/python

import os
import re
import sys

try:
   startTime=sys.argv[1]
except:
   startTime="0"

numStartTime=float(startTime)

files = []

for (pathname,dirname,filenames) in os.walk("postProcessing/forces/"):     
  for filename in filenames:
    files.append(os.path.join(pathname, filename))

files.sort()
#print files

output=open("xforces.dat","w")

for file in files:

  iter=0
  input=open(file,"r")

  for lines in input:
    if(iter>=5):

      newLines=lines.replace("("," ")
      newLines=newLines.replace(")"," ")

      try:
        if(float(newLines.split()[0]) > numStartTime):
          output.write(newLines.split()[0]+" "+newLines.split()[1]+" "+newLines.split()[4]+"\n")
      except ValueError:
         break 
       
    iter=iter+1

input.close()
output.close()

os.system("gnuplot plot_forces.p")

os.system("epstopdf forces.ps")
os.system("evince forces.pdf &")


