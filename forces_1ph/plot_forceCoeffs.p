reset

set terminal postscript eps color enhanced "Helvetica,20"  size 5,4
set autoscale

set key width +2
lW = 2

set title plotTitle
set key top right
set key reverse
set key width +2

set grid
set xlabel "t"
set ylabel "-"

set yrange [*:*]
set xrange [xmin:xmax]

set xtics 1.0

set output outputFile

set autoscale
set yr[*:*]
set title 'Total resistance coefficient'

plot inputFile  u 1:3 with lines title "C_T" 


