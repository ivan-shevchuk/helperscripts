#! /bin/python

try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()
import os
#import showmsg

#print os.getcwd()

for i in range(0,len(GetRenderViews())):
    WriteImage(os.getcwd()+"/screenshot_%d.png" % (i), GetRenderViews()[i], Writer="vtkPNGWriter", Magnification=1 )  

os.system("showmsg.py &")
