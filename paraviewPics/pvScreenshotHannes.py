try: paraview.simple
except: from paraview.simple import *

paraview.simple._DisableFirstRenderCameraReset()
import os,sys,re

#print os.getcwd()
def isfloat(x):
 try:
  float(x)
  return True;
 except:
  return False;

times=sorted(map(float, filter(isfloat, os.listdir("."))))
print times
#curtime=times[-1]
servermanager.LoadState("state.pvsm")

AnimationScene1 = GetAnimationScene()
AnimationScene1.EndTime = times[-1]

for curtime in times:
 AnimationScene1.AnimationTime = curtime

 for i in range(0,len(GetRenderViews())):
    WriteImage(os.getcwd()+"/screenshot_%d_t%g.png" % (i,curtime), GetRenderViews()[i], Writer="vtkPNGWriter", Magnification=1 )  

