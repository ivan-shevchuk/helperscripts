#~ try: paraview.simple
#~ except: from paraview.simple import *

import sys
import os
from os import path
from os.path import join
from math import *
from quaternion import *
import subprocess as sp
import numpy as np
import cutWithPlane
import argparse
import pvopenstate
import glob as gb


from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile, WriteParameterFile
from PyFoam.Basics.TemplateFile import *
from PyFoam.Execution.UtilityRunner import UtilityRunner
from PyFoam.Execution.BasicRunner import BasicRunner
from PyFoam.Basics.DataStructures import *

#paraview.simple._DisableFirstRenderCameraReset()

splittingBoxDict = ParsedParameterFile(join("./","functions","probesProp1"),noHeader=True)

#print type(splittingBoxDict)
allPoints = []
for entry in splittingBoxDict:
    allPoints.append(np.array(splittingBoxDict[entry]["probeLocations"]))
    
splittingBoxDict = ParsedParameterFile(join("./","functions","probesProp2"),noHeader=True)

for entry in splittingBoxDict:
    allPoints.append(np.array(splittingBoxDict[entry]["probeLocations"]))
    
#~ print allPoints
for line in allPoints:
    #~ print line
    for point in line:
        #~ print point.tolist()
        SphereI = Sphere()
        SetActiveSource(SphereI)
        SphereI.Radius = 0.025
        SphereI.Center = point.tolist()
        DataRepresentationI = GetDisplayProperties( SphereI )
        DataRepresentationI.DiffuseColor = [0.8980392156862745, 0.0, 0.011764705882352941]

#~ Sphere7 = Sphere()
#~ 
#~ Sphere6 = FindSource( "Sphere6" )
#~ 
#~ SetActiveSource(Sphere6)
#~ RenderView1 = GetRenderView()
#~ 
#~ points = {(  3.065 1.65 -1.165  )
 #~ 
#~ 
#~ 
#~ 
             #~ (  3.065 1.65 -1.165  )
             #~ (  3.065 1.65 -1.06  )
             #~ (  3.065 1.65 -0.955  )
             #~ (  3.065 1.65 -0.85  )
             #~ (  3.065 1.86 -1.375  )
             #~ (  3.065 1.965 -1.375  )
             #~ (  3.065 2.07 -1.375  )
             #~ (  3.065 2.175 -1.375  )
             #~ (  3.065 1.65 -1.585  )
             #~ (  3.065 1.65 -1.69  )
             #~ (  3.065 1.65 -1.795  )
             #~ (  3.065 1.65 -1.9  )
             #~ (  3.065 1.44 -1.375  )
             #~ (  3.065 1.335 -1.375  )
             #~ (  3.065 1.23 -1.375  )
             #~ (  3.065 1.125 -1.375  )
#~ } = GetActiveSource()
#~ p4.Radius = 0.025
#~ 
#~ DataRepresentation5 = GetDisplayProperties( p4 )
#~ DataRepresentation5.DiffuseColor = [0.8980392156862745, 0.0, 0.011764705882352941]
#~ 
#~ SetActiveSource(Sphere4)
#~ DataRepresentation2 = Show()
#~ DataRepresentation2.EdgeColor = [0.0, 0.0, 0.5019607843137255]
#~ DataRepresentation2.SelectionPointFieldDataArrayName = 'Normals'
#~ DataRepresentation2.DiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation2.BackfaceDiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation2.ScaleFactor = 0.1
#~ 
#~ Sphere7.Center = [3.065, 1.65, -1.69]
#~ 
#~ SetActiveSource(Sphere7)
#~ DataRepresentation3 = Show()
#~ DataRepresentation3.EdgeColor = [0.0, 0.0, 0.5019607843137255]
#~ DataRepresentation3.SelectionPointFieldDataArrayName = 'Normals'
#~ DataRepresentation3.DiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation3.BackfaceDiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation3.ScaleFactor = 0.1
#~ 
#~ Sphere2 = FindSource( "Sphere2" )
#~ 
#~ SetActiveSource(Sphere2)
#~ DataRepresentation4 = Show()
#~ DataRepresentation4.EdgeColor = [0.0, 0.0, 0.5019607843137255]
#~ DataRepresentation4.SelectionPointFieldDataArrayName = 'Normals'
#~ DataRepresentation4.DiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation4.BackfaceDiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation4.ScaleFactor = 0.1
#~ 
#~ Sphere5 = FindSource( "Sphere5" )
#~ 
#~ SetActiveSource(Sphere5)
#~ DataRepresentation5 = Show()
#~ DataRepresentation5.EdgeColor = [0.0, 0.0, 0.5019607843137255]
#~ DataRepresentation5.SelectionPointFieldDataArrayName = 'Normals'
#~ DataRepresentation5.DiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation5.BackfaceDiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation5.ScaleFactor = 0.1
#~ 
#~ Sphere3 = FindSource( "Sphere3" )
#~ 
#~ SetActiveSource(Sphere3)
#~ DataRepresentation6 = Show()
#~ DataRepresentation6.EdgeColor = [0.0, 0.0, 0.5019607843137255]
#~ DataRepresentation6.SelectionPointFieldDataArrayName = 'Normals'
#~ DataRepresentation6.DiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation6.BackfaceDiffuseColor = [1.0, 0.9333333333333333, 0.1607843137254902]
#~ DataRepresentation6.ScaleFactor = 0.1
#~ 
#~ RenderView1.CameraClippingRange = [123.71795266611088, 434.75451222192606]
#~ 
#~ Render()
