
#! /bin/python

try: paraview.simple
except: from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()
import os

#print os.getcwd()

for i in range(0,len(GetRenderViews()))
    path=os.getcwd()
    layout="view_"+str(i)
    SetRenderView(GetRenderViews()[i])
    WriteAnimation(layout+".jpg", Magnification=1, Quality=2, FrameRate=15.000000)
    #WriteImage(os.getcwd()+"/screenshot_%d.png" % (i), GetRenderViews()[i], Writer="vtkPNGWriter", Magnification=1 )  

Render()
