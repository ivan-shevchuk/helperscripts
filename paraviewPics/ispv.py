#! /bin/python

#try: paraview.simple
#except: from paraview.simple import *
#paraview.simple._DisableFirstRenderCameraReset()

#My version of pyFoamPVLoadState script, which loads a paraview state using data from the current directory

from xml.dom.minidom import parse
import xml.dom
from os import path
import os
import shutil
import glob
import sys
import re

###################################################################################

class Proxy(object):
    """Convenience class for handling proxies"""
    def __init__(self,xml):
        self.data=xml
        
    def setProperty(self,name,value,index=None):
        """Set a property in a proxy

        @param name: name of the property
        @param value: the new value
        @param index: Index. If not specified all elements are changed"""
        
        for p in self.data.getElementsByTagName("Property"):
            if p.getAttribute("name")==name:
                for e in p.getElementsByTagName("Element"):
                    if index==None or index==int(e.getAttribute("index")):
                        e.setAttribute("value",str(value))

    def getProperty(self,name,index=None):
        """Get a property in a proxy

        @param name: name of the property
        @param index: Index. If not specified all elements are changed"""
        
        for p in self.data.getElementsByTagName("Property"):
            if p.getAttribute("name")==name:
                for e in p.getElementsByTagName("Element"):
                    if index==None or index==int(e.getAttribute("index")):
                        return e.getAttribute("value")
        return None
    
    def rewriteProperty(self,name,values,index=None):
        """Rewrites a property by replacing all strings of the form %%(key)s
        (Python-notation for dictionary-replacement) with a corresponding value

        @param name: name of the property
        @param values: Dictionary with the keys and the corresponding values
        @param index: Index. If not specified all elements are changed"""

        for p in self.data.getElementsByTagName("Property"):
            if p.getAttribute("name")==name:
                for e in p.getElementsByTagName("Element"):
                    if index==None or index==int(e.getAttribute("index")):
                        old = e.getAttribute("value")
                        new = old % values
                        if new!=old:
                            # print "Replacing",old,"with",new
                            e.setAttribute("value",new)

###############################################################################

scriptPath = os.path.join(os.environ.get("SCRIPT_TEMPL"),"paraviewPics")

try:
   stateFileName=sys.argv[1]
except:
   stateFileName="void.pvsm"
   
try:
   screenshotGeneratorScript=sys.argv[2]
   screenshotGeneratorScript=os.path.join(scriptPath,screenshotGeneratorScript)
except:
   screenshotGeneratorScript=os.path.join(scriptPath,"pvScreenGen.py")

case=os.getcwd()
short=path.basename(case)
fName=path.join(case,short+".OpenFOAM")

createdDataFile=False

if not path.exists(fName):
    createdDataFile=True
    f=open(fName,"w")
    f.close()

"""Parsing .pvsm file and setting new case directory"""

stateFileFullPath = os.getcwd()+"/"+stateFileName
print stateFileFullPath

stateFile=parse(stateFileFullPath)

doc=stateFile.documentElement

serverState = doc.getElementsByTagName("ServerManagerState")[0]

proxiesPV4Foam=[]

for p in serverState.getElementsByTagName("Proxy"):
    tp=p.getAttribute("type")
    if "PV4FoamReader"==tp:
        proxiesPV4Foam.append(Proxy(p))

proxiesOpenFOAMReader=[]

for p in serverState.getElementsByTagName("Proxy"):
    tp=p.getAttribute("type")
    if "OpenFOAMReader"==tp:
        proxiesOpenFOAMReader.append(Proxy(p))

reader=(proxiesPV4Foam+proxiesOpenFOAMReader)[0]

typ=reader.data.getAttribute("type")

if typ=="PV4FoamReader":
    newFile=path.join(path.dirname(case),fName)
    reader.setProperty("FileName",newFile)
    print newFile
elif typ=="OpenFOAMReader":
    oldFile=reader.getProperty("FileName")
    fName=path.basename(oldFile)
    newFile=path.join(path.dirname(case),fName)
    print newFile
    if not path.exists(newFile):
        open(newFile,"w")
    reader.setProperty("FileName",newFile)
    
    
open(stateFileFullPath,"w").write(stateFile.toxml())

saveScreenshot = True

if saveScreenshot:
    os.system("paraview"+" --state="+stateFileName+" --script="+screenshotGeneratorScript)
else:
   os.system("paraview"+" --state="+stateFileName)
